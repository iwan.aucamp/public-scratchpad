= ...

== latest

----
# I run this inside ~/xincoming/mobilink.pk/201510/
# I have symlink ~/xincoming/mobilink.pk/201510/gnuplot-5.0-js -> /usr/share/gnuplot/5.0/js
# i.e. ln -s /usr/share/gnuplot/5.0/js ~/xincoming/mobilink.pk/201510/gnuplot-5.0-js
# The exact gnuplot js path might differ on other systems


echo hxc{svc-a{01..08},slc-a{01..12}} | tr ' ' '\n' | xargs -P 5 -t -I{} rsync -avz {}.mobilink.pk:/var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/prstat.log-201510* {}

for server in hxc{svc-a{01..08},slc-a{01..12}}; do
( cd ${server} && find prstat.log-???????????? | xargs -n1 bash -c 'test "${1}" -nt "svccore-${1}.txt" && echo "${1}"' /dev/null | xargs -t -r gawk '/svccore/{ print $0 > "svccore-"FILENAME".txt" }' )
done

find hxc*/ -name 'svccore-prstat.log-*.txt' -type f | sed -n 's,^\(.*\)\/\(svccore-prstat.log-\)\([^/]\{8\}\)[^/]*$,\1 \2 \3,gp' | sort | uniq | gxargs -n3 bash -c 'cat "${1}/${2}${3}"*.txt | /var/opt/disk000/incoming/mobilink.pk/prstat-vm-plot -y "0 : 70000000000.0" -x "\"${3}T000000\":\"${3}T235959\"" -f svg > "${1}-${2}${3}".svg' /dev/null

find hxc*/ -name 'svccore-prstat.log-*.txt' -type f | sed -n 's,^\(.*\)\/\(svccore-prstat.log-\)\([^/]\{8\}\)[^/]*$,\1 \2 \3,gp' | sort | uniq | gxargs -n3 bash -c 'cat "${1}/${2}${3}"*.txt | /var/opt/disk000/incoming/mobilink.pk/prstat-vm-plot -y "0 : 70000000000.0" -x "\"${3}T000000\":\"${3}T235959\"" -f canvas > "${1}-${2}${3}".html' /dev/null

rsync -avz --no-owner --no-group --no-links --copy-links --no-perms gnuplot-5.0-js *-svccore-prstat.log-* ssh.concurrent.co.za:/var/opt/webdav/warehouse.concurrent.co.za/sites/mobilink.pk/graphs/
----

== old


----
gfind /var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/ -iregex '^.*/prstat.log-2015\(07\|08\|09\).*$' | sort | gxargs ggrep -h svccore > ~cs/iwana/prstat.log-2015_07_08_09-svccore.txt

gawk 'BEGIN{ p_pid=0; p_line="" } { c_pid=$4; c_line=$0; if ( p_pid != 0 && p_pid != c_pid ) { print p_line; print c_line }; p_pid=c_pid; p_line=c_line; } END{ print }' ~cs/iwana/prstat.log-2015_07_08_09-svccore.txt

cat prstat.log-2015_07_08_09-svccore.txt | gawk '{ date=gensub( "^(.*)T.*$", "\\1", "g", $0 ); print $0 > "prstat.log-"date"-svccore.txt"; fflush() }'


for file in prstat.log-20150???-svccore.txt; do cat ${file} | ./prstat-vm-plot -f svg > ${file}.svg; done;
for file in prstat.log-20150???-svccore.txt; do cat ${file} | ./prstat-vm-plot -f canvas > ${file}.html; done;

cat prstat.log-2015_07_08_09-svccore.txt | ./prstat-vm-plot -f svg > prstat.log-2015_07_08_09-svccore.txt.svg
cat prstat.log-2015_07_08_09-svccore.txt | ./prstat-vm-plot -f canvas > prstat.log-2015_07_08_09-svccore.txt.html

rsync -avz --no-owner --no-group --no-links --copy-links --no-perms gnuplot-5.0-js prstat.log-2015* ssh.concurrent.co.za:/var/opt/webdav/support-wh.concurrent.co.za/sites/mobilink.pk/201509/

rsync -avz --no-owner --no-group --no-links --copy-links --no-perms gnuplot-5.0-js prstat.log-2015* ssh.concurrent.co.za:/var/opt/webdav/warehouse.concurrent.co.za/sites/mobilink.pk/201509/

cat prstat.log-2015_07_08_09-svccore.txt | gawk 'BEGIN{ p_pid=0; p_line="" } { c_pid=$4; c_line=$0; if ( p_pid != 0 && p_pid != c_pid ) { print p_line; print c_line }; p_pid=c_pid; p_line=c_line; } END{ print }' > prstat.log-2015_07_08_09-svccore.txt.summary.txt

----



----
for server in hxc{svc-a{01..08},slc-a{01..12}}; do rsync -avz ${server}.mobilink.pk:/var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/prstat.log-201509{28,29,30}* ${server}/; done

echo hxc{svc-a{01..08},slc-a{01..12}} | tr ' ' '\n' | xargs -P 5 -t -I{} rsync -avz {}.mobilink.pk:/var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/prstat.log-201509{28,29,30}* {}


for server in hxc{svc-a{01..08},slc-a{01..12}}; do rsync -avz ${server}.mobilink.pk:/var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/prstat.log-20151001* ${server}/; done

echo hxc{svc-a{01..08},slc-a{01..12}} | tr ' ' '\n' | xargs -P 5 -t -I{} rsync -avz {}.mobilink.pk:/var/cs/installations/countries/pk/plmns/mobilink/products/stats/instances/prod/log/prstat.log-20151001* {}


for server in hxc{svc-a{01..08},slc-a{01..12}}; do
( cd ${server} && find prstat.log-???????????? | xargs -n1 bash -c 'test "${1}" -nt "svccore-${1}.txt" && echo "${1}"' /dev/null | xargs -t -r gawk '/svccore/{ print $0 > "svccore-"FILENAME".txt" }' )
done

find hxc*/ -name 'svccore-prstat.log-*.txt' -type f | sed -n 's,^\(.*\)\/\(svccore-prstat.log-\)\([^/]\{8\}\)[^/]*$,\1 \2\3,gp' | sort | uniq | gxargs -n2 bash -c 'cat "${1}/${2}"*.txt | /var/opt/disk000/incoming/mobilink.pk/prstat-vm-plot -f svg > "${1}-${2}".svg' /dev/null

find hxc*/ -name 'svccore-prstat.log-*.txt' -type f | sed -n 's,^\(.*\)\/\(svccore-prstat.log-\)\([^/]\{8\}\)[^/]*$,\1 \2\3,gp' | sort | uniq | gxargs -n2 bash -c 'cat "${1}/${2}"*.txt | /var/opt/disk000/incoming/mobilink.pk/prstat-vm-plot -f canvas > "${1}-${2}".html' /dev/null


rsync -avz --no-owner --no-group --no-links --copy-links --no-perms gnuplot-5.0-js *-svccore-prstat.log-* ssh.concurrent.co.za:/var/opt/webdav/warehouse.concurrent.co.za/sites/mobilink.pk/graphs/

----


