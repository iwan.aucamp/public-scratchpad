#!/bin/bash

script_basename=$(basename "${0}")
script_dirname=$(dirname "${0}")

expand_boolean()
{
	local boolean_name=${1}
	local boolean_value
	eval "boolean_value=\${${boolean_name}}"
	if "${boolean_value}"
	then
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

expand_test()
{
	local boolean_name=${1}
	shift 1
	if "${@}"
	then
		eval "${boolean_name}=true"
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}=false"
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

do_if()
{
	:
}

verbose_echo()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
	echo "${@}" 1>&2
	return 0
}

verbose_dump_vars()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_vars_msg()
{
	local level="${1}"
	local msg="${2}"
	shift 2
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ ${msg} " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_functions()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for function in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -f "${function}" 1>&2
	done
	return 0
}

error_echo()
{
	echo "${@}" 1>&2
}

arcsnoop_main_usage()
{
	stty tabs
	test "${#}" -ge 1 && { error_echo "ERROR: ${@}"; }
	local align="\r\t\t\t"
	error_echo -e "${script_basename} [-f (svg|canvas)]"
	return 0;
}

prstat_vm_plot_main()
{
	local data=$(cat - | gawk '{ iecp["K"]=1024; iecp["M"]=1024*iecp["K"]; iecp["G"]=1024*iecp["M"]; }{ for ( i = 6; i<=7; ++i ) { $i=( substr( $i,1,length( $i ) - 1 ) * iecp[ substr( $i, length( $i ) ) ] ) }; print $0 }')
	local argv=( "${@}" )
	local verbosity=-1
	local verbose=false

	local format=""

	local xrange=""
	local yrange=""

	local OPTARG OPTERR option OPTIND
	while getopts "vhf:x:y:" option "${@}"
	do
		case "${option}" in
			f)
				format="${OPTARG}"
				;;
			h)
				"${FUNCNAME}_usage"
				return 0;
				;;
			v)
				verbosity=$(( ${verbosity} + 1 ))
				;;
			x)
				xrange="${OPTARG}"
				;;
			y)
				yrange="${OPTARG}"
				;;
			*)
				"${FUNCNAME}_usage" "Invalid options [ OPTARG=${OPTARG}, OPTERR=${OPTERR}, option=${option} ]"
				return 0;
				;;
		esac
	done
	test "${verbosity}" -ge 0 && { verbose=true; }
	shift $(( ${OPTIND} - 1 ))
	verbose_echo 1 "${FUNCNAME}: @=${argv}"
	verbose_dump_vars 1 format xrange yrange

	{ cat <<EOF
#set term wxt persist 0
#set term svg enhanced mouse size 1920,1080
#set term canvas standalone mousing enhanced mouse size 1920,1080
$( test "${format}" = "svg" && echo "set term svg enhanced mouse jsdir './gnuplot-5.0-js/' size 1280,720")
$( test "${format}" = "canvas" && echo "set term canvas standalone mousing enhanced mouse size 1280,720 jsdir './gnuplot-5.0-js/'")
set title "vsize/rss"
set style data lines

set timefmt "%Y%m%dT%H%M%S"
set format x "%m-%d\n%H:%M"
set format y "%.1s%c"

set ylabel "Size"
$( test -n "${yrange}" && echo "set yrange [ ${yrange} ]" )
#set yrange [ 0 : 70000000000.0 ]

set xlabel "Date\nTime"
set xdata time
$( test -n "${xrange}" && echo "set xrange [ ${xrange} ]" )
#set xrange [ "1/6/93": "" ]

set grid
set key left

plot "-" using 1:6 title "vsize", \
	"" using 1:7 title "rss"
${data}
e
${data}
EOF
	} | gnuplot -p /dev/stdin

}

prstat_vm_plot_main "${@}"
exit "${?}"
