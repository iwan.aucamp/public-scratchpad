= ...


----
# http://mirror.opencsw.org/opencsw/testing/sparc/5.10/pkgutil-2.6.7%2cREV%3d2014.10.16-SunOS5.10-all-CSW.pkg.gz
# http://get.opencsw.org/now
wget http://get.opencsw.org/now -O opencsw-pkgutil.pkg
uniq=$(date +%Y%m%d%H%M%S)-$(cat /dev/urandom | tr -d -c '[:digit:]' | dd bs=8 count=1 if=/dev/stdin of=/dev/stdout 2>/dev/null )
fld=/tmp/${uniq}
mkdir -p ${fld}
pkgtrans opencsw-pkgutil.pkg ${fld} all

proto=${fld}/CSWpkgutil
find ${proto}

root=${proto}/root
find ${root}

csw_local="${HOME}/.csw-local"
mkdir -p "${csw_local}"
find ${csw_local}

awk '( $2 == "d" ){ print $4 }' ${proto}/pkgmap | xargs -I{} echo mkdir -p "${csw_local}"{}
awk '( $2 == "d" ){ print $4 }' ${proto}/pkgmap | xargs -I{} -t mkdir -p "${csw_local}"{}
find ${csw_local}

find ${root} -type d | sed "s,${root},,g;s,^/,,g" | xargs -I{} echo mkdir -p "${csw_local}/"{}
find ${root} -type d | sed "s,${root},,g;s,^/,,g" | xargs -t -I{} mkdir -p "${csw_local}/"{}
find ${csw_local}

find ${root} \! -type d | sed "s,${root},,g;s,^/,,g" | xargs -I{} echo cp "${root}/"{} "${csw_local}/"{}
find ${root} \! -type d | sed "s,${root},,g;s,^/,,g" | xargs -I{} -t cp "${root}/"{} "${csw_local}/"{}
find ${csw_local}

PKG_INSTALL_ROOT="${csw_local}" bash -x ${proto}/install/postinstall
PKG_INSTALL_ROOT="${csw_local}" bash ${proto}/install/postinstall
find ${csw_local}

MANPATH="${csw_local}/opt/csw/share/man" man pkgutil

${csw_local}/opt/csw/bin/pkgutil -U
${csw_local}/opt/csw/bin/pkgutil -W "${HOME}/.pkgutil/" -R "${csw_local}" -U
${csw_local}/opt/csw/bin/pkgutil --config "${csw_local}/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${csw_local}" -U
mkdir -p ${csw_local}/var/sadm/pkg
${csw_local}/opt/csw/bin/pkgutil --config "${csw_local}/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${csw_local}" -l | grep gcc4
${csw_local}/opt/csw/bin/pkgutil --config "${csw_local}/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${csw_local}" -a | grep gcc4
${csw_local}/opt/csw/bin/pkgutil --config "${csw_local}/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${csw_local}" -i gcc4g++
${csw_local}/opt/csw/bin/pkgutil --config "${csw_local}/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${csw_local}" -d -s -o gcc4g++.pkgstream gcc4g++

find "${HOME}/.csw-local"

${HOME}/.csw-local/opt/csw/bin/pkgutil --config "${HOME}/.csw-local/etc/opt/csw/pkgutil.conf" -W "${HOME}/.pkgutil/" -R "${HOME}/.csw-local" -a | grep boost
----

# LD_LIBRARY_PATH before RPATH

----
Install commands in dependency safe order:

pkgadd -d gcc4g++.pkgstream CSWcommon
pkgadd -d gcc4g++.pkgstream CSWlibiconv2
pkgadd -d gcc4g++.pkgstream CSWlibcharset1
pkgadd -d gcc4g++.pkgstream CSWggettext-data
pkgadd -d gcc4g++.pkgstream CSWiconv
pkgadd -d gcc4g++.pkgstream CSWlibintl8
pkgadd -d gcc4g++.pkgstream CSWcas-texinfo
pkgadd -d gcc4g++.pkgstream CSWlibpcre1
pkgadd -d gcc4g++.pkgstream CSWlibgcc-s1
pkgadd -d gcc4g++.pkgstream CSWlibz1
pkgadd -d gcc4g++.pkgstream CSWlibgcj15
pkgadd -d gcc4g++.pkgstream CSWlibstdc++6
pkgadd -d gcc4g++.pkgstream CSWgsed
pkgadd -d gcc4g++.pkgstream CSWggrep
pkgadd -d gcc4g++.pkgstream CSWbinutils
pkgadd -d gcc4g++.pkgstream CSWlibssp0
pkgadd -d gcc4g++.pkgstream CSWlibitm1
pkgadd -d gcc4g++.pkgstream CSWlibgomp1
pkgadd -d gcc4g++.pkgstream CSWlibgo5
pkgadd -d gcc4g++.pkgstream CSWlibgij15
pkgadd -d gcc4g++.pkgstream CSWlibgcj-tools15
pkgadd -d gcc4g++.pkgstream CSWlibatomic1
pkgadd -d gcc4g++.pkgstream CSWlib-gnu-awt-xlib15
pkgadd -d gcc4g++.pkgstream CSWalternatives
pkgadd -d gcc4g++.pkgstream CSWlibgmp10
pkgadd -d gcc4g++.pkgstream CSWcoreutils
pkgadd -d gcc4g++.pkgstream CSWlibmpfr4
pkgadd -d gcc4g++.pkgstream CSWlibmpc3
pkgadd -d gcc4g++.pkgstream CSWgcc4core
pkgadd -d gcc4g++.pkgstream CSWgcc4g++


pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWcommon
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibiconv2
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibcharset1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWggettext-data
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWiconv
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibintl8
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWcas-texinfo
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibpcre1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgcc-s1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibz1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgcj15
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibstdc++6

pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWgsed
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWggrep
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWbinutils
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibssp0
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibitm1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgomp1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgo5

pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgij15
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgcj-tools15
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibatomic1
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlib-gnu-awt-xlib15
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWalternatives

pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibgmp10
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWcoreutils
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibmpfr4
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWlibmpc3
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWgcc4core
pkgadd_pretend=true ./pkgadd -d ./gcc4g++.d/ CSWgcc4g++


./pkgadd -d ./gcc4g++.d/ CSWcommon
./pkgadd -d ./gcc4g++.d/ CSWlibiconv2
./pkgadd -d ./gcc4g++.d/ CSWlibcharset1
./pkgadd -d ./gcc4g++.d/ CSWggettext-data
./pkgadd -d ./gcc4g++.d/ CSWiconv
./pkgadd -d ./gcc4g++.d/ CSWlibintl8
./pkgadd -d ./gcc4g++.d/ CSWcas-texinfo
./pkgadd -d ./gcc4g++.d/ CSWlibpcre1
./pkgadd -d ./gcc4g++.d/ CSWlibgcc-s1
./pkgadd -d ./gcc4g++.d/ CSWlibz1
./pkgadd -d ./gcc4g++.d/ CSWlibgcj15
./pkgadd -d ./gcc4g++.d/ CSWlibstdc++6

./pkgadd -d ./gcc4g++.d/ CSWgsed
./pkgadd -d ./gcc4g++.d/ CSWggrep
./pkgadd -d ./gcc4g++.d/ CSWbinutils
./pkgadd -d ./gcc4g++.d/ CSWlibssp0
./pkgadd -d ./gcc4g++.d/ CSWlibitm1
./pkgadd -d ./gcc4g++.d/ CSWlibgomp1
./pkgadd -d ./gcc4g++.d/ CSWlibgo5

./pkgadd -d ./gcc4g++.d/ CSWlibgij15
./pkgadd -d ./gcc4g++.d/ CSWlibgcj-tools15
./pkgadd -d ./gcc4g++.d/ CSWlibatomic1
./pkgadd -d ./gcc4g++.d/ CSWlib-gnu-awt-xlib15
./pkgadd -d ./gcc4g++.d/ CSWalternatives

./pkgadd -d ./gcc4g++.d/ CSWlibgmp10
./pkgadd -d ./gcc4g++.d/ CSWcoreutils
./pkgadd -d ./gcc4g++.d/ CSWlibmpfr4
./pkgadd -d ./gcc4g++.d/ CSWlibmpc3
./pkgadd -d ./gcc4g++.d/ CSWgcc4core
./pkgadd -d ./gcc4g++.d/ CSWgcc4g++

export PATH="${HOME}/.csw-local/opt/csw/bin:${HOME}/.csw-local/opt/csw/sbin:${HOME}/.csw-local/opt/csw/gcc4/bin:${HOME}/.local/bin/:/usr/ccs/bin/:${PATH}"
export LD_LIBRARY_PATH="${HOME}/.csw-local/opt/csw/lib/sparcv9:${HOME}/.csw-local/opt/csw/lib:${LD_LIBRARY_PATH}"
export MANPATH="${HOME}/.csw-local/opt/csw/man:${MANPATH}"

----

