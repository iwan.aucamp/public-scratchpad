#!/bin/bash

script_basename=$(basename "${0}")
script_dirname=$(dirname "${0}")

expand_boolean()
{
	local boolean_name=${1}
	local boolean_value
	eval "boolean_value=\${${boolean_name}}"
	if "${boolean_value}"
	then
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

expand_test()
{
	local boolean_name=${1}
	shift 1
	if "${@}"
	then
		eval "${boolean_name}=true"
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}=false"
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

do_if_true()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		"${@}"
		return "${?}"
	else
		return 1
	fi
}

do_if_false()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		return 1
	else
		"${@}"
		return "${?}"
	fi
}

verbosity_to_flags()
{
	local flag="${1}"
	local remainder="${verbosity:--1}"
	while [ "${remainder}" -gt "-1" ]
	do
		remainder=$(( ${remainder} - 1 ))
		echo -n " ${flag}"
	done
	echo
}

verbose_echo()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
	echo "${@}" 1>&2
	return 0
}

verbose_dump_vars()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_vars_msg()
{
	local level="${1}"
	local msg="${2}"
	shift 2
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ ${msg} " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_functions()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for function in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -f "${function}" 1>&2
	done
	return 0
}

error_echo()
{
	echo "${@}" 1>&2
}

ussdgw_alarm_main_usage()
{
	stty tabs
	test "${#}" -ge 1 && { error_echo "ERROR: ${@}"; }
	local align="\r\t\t\t"
	error_echo -e "${script_basename} [options] [interval] [count]"
	error_echo -e "options:"
	error_echo -e " -h${align}help"
	error_echo -e " -v${align}increase verbosity"
	error_echo -e " -P${align}pretend mode"
	error_echo -e " -p pid${align}target pid"
	error_echo -e " -c cmd${align}target command"
	return 0;
}

expand_probesets()
{
	local probesets=( "${@}" )
	local probes=""
	for probeset_name in "${probesets[@]}"
	do
		local probeset=""
		eval 'probeset="${probeset_'${probeset_name}'}"'
		verbose_dump_vars 1 probeset
		test -n "${probeset}" || { error_echo "Invalid probeset name ${probeset_name}"; return 1;}
		probes="${probes:+${probes}$',\n'}${probeset}"
	done
	echo "${probes}"
	return 0
}

ussdgw_alarm_main()
{
	local argv=( "${@}" )
	local verbosity=-1
	local verbose=false
	local pretend=false

	local target_pid=
	local target_cmd=

	verbose_echo 1 "${FUNCNAME}: @=${@}"
	local OPTARG OPTERR option OPTIND
	while getopts "hvPp:c:" option "${@}"
	do
		case "${option}" in
			h)
				"${FUNCNAME}_usage"
				return 0;
				;;
			v)
				verbosity=$(( ${verbosity} + 1 ))
				;;
			P)
				pretend=true;
				;;
			c)
				target_cmdcmd=${OPTARG};
				;;
			p)
				target_pid=${OPTARG};
				;;
			*)
				"${FUNCNAME}_usage" "Invalid options [ OPTARG=${OPTARG}, OPTERR=${OPTERR}, option=${option} ]"
				exit 1
				;;
		esac
	done
	test "${verbosity}" -ge 0 && { verbose=true; }
	verbose_dump_vars 1 script_dirname script_basename argv OPTERR option OPTIND
	verbose_dump_vars 1 verbosity pretend
	shift $(( ${OPTIND} - 1 ))

	verbose_dump_vars 1 target_cmd target_pid

	local target_pid_set target_pid_set_true target_pid_set_false
	expand_test target_pid_set test -n "${target_pid}"
	verbose_dump_vars 1 target_pid_set target_pid_set_true target_pid_set_false

	local target_cmd_set target_cmd_set_true target_cmd_set_false
	expand_test target_cmd_set test -n "${target_cmd}"
	verbose_dump_vars 1 target_cmd_set target_cmd_set_true target_cmd_set_false

	{ ${target_pid_set} || ${target_cmd_set}; } || {
		${FUNCNAME}_usage "need either -c cmd or -p pid" ; return 1;
	}
	{ ${target_pid_set} && ${target_cmd_set}; } && {
		${FUNCNAME}_usage "cannot have both -c cmd and -p pid" ; return 1;
	}

	local pretend_true pretend_false
	expand_boolean pretend
	verbose_dump_vars 1 pretend_true pretend_false

	false <<COMMENT
		dtrace -ln 'pid$target:::entry' -p `pgrep -x ussdgw_hd3`
COMMENT

	ru_pf_t=""
	ru_pv_t=""

	ru_pf_t="${ru_pf_t}${ru_pf_t:+ | }wt %d.%09d di %d.%09d du %d.%09d"
	ru_pv_t="${ru_pv_t}${ru_pv_t:+, }walltimestamp / 1000000000, walltimestamp % 1000000000"
	ru_pv_t="${ru_pv_t}${ru_pv_t:+, }self->diff / 1000000000, self->diff % 1000000000"
	ru_pv_t="${ru_pv_t}${ru_pv_t:+, }self->duration / 1000000000, self->duration % 1000000000"

	ru_pf_t="${ru_pf_t}${ru_pf_t:+ | }%s:%s:%s:%s"
	ru_pv_t="${ru_pv_t}${ru_pv_t:+, }probeprov, probemod, probefunc, probename"

	ru_pf_t="${ru_pf_t}${ru_pv_t:+ | }x %s p %i t %i | %i %i %i"
	ru_pv_t="${ru_pv_t}${ru_pv_t:+, }execname, pid, tid, self->indent, self->speculation, self->tracing"

	program='
self int tracing;
self int indent;
self int speculation;
self int captured_entry;
self uint64_t entry_timing[ string, string, string, string ];
self uint64_t entry_previous;
self uint64_t return_previous;
self uint64_t diff;
self uint64_t duration;

'"${comment_entry}"'

pid$target:ussdgw_hd3:_ZN2cs3sgw6ussdgw14MAPHandlerImplINS1_11MapImplETSIEE13waitForEventsEi:entry
{
	/*
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	self->tracing = self->tracing + 1;
	*/
	self->tracing = 1;
	self->indent = 1;
}

pid$target:ussdgw_hd3::entry,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::entry
/self->tracing && !self->captured_entry/
{
	self->diff = timestamp - self->entry_previous;
	self->entry_previous = timestamp;

	self->timing_entry[ probeprov, probemod, probefunc ] = timestamp;
	self->duration = 0;
}

pid$target:ussdgw_hd3::entry,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::entry
/self->tracing && !self->captured_entry && self->indent <= 10/
{
	self->indent = self->indent + 1;
	printf( "%*s '"${ru_pf_t}"' |\n", self->indent, "{"'"${ru_pv_t:+, }${ru_pv_t}"' );
}

pid$target:ussdgw_hd3::entry,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::entry
/self->tracing && !self->captured_entry/
{
	self->captured_entry = 0;
}

'"${comment_inside}"'

'"${comment_return}"'

pid$target:ussdgw_hd3::return,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::return
/self->tracing && !self->captured_return/
{
	self->diff = timestamp - self->return_previous;
	self->return_previous = timestamp;

	self->duration = timestamp - self->timing_entry[ probeprov, probemod, probefunc ];
}

pid$target:ussdgw_hd3::return,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::return
/self->tracing && !self->captured_return && self->indent <= 10/
{
	printf( "%*s '"${ru_pf_t}"' |\n", self->indent, "}"'"${ru_pv_t:+, }${ru_pv_t}"' );
	self->indent = self->indent - 1;
}

pid$target:ussdgw_hd3::return,
pid$target:librouter-csutils.so.4.0.0e2461s123522ha54b::return
/self->tracing && self->captured_return/
{
	self->captured_return = 0;
}

/*
pid$target:ussdgw_hd3:_ZN2cs3sgw6ussdgw14MAPHandlerImplINS1_11MapImplETSIEE13waitForEventsEi:return
/self->tracing/
{
	self->tracing = self->tracing - 1;
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
}
*/


'"${comment_other}"'

pid$target::_ZN2cs3sgw6ussdgw11entry_pointEiPPc:
{
	printf( "%*s'"${ru_pf_t}"'(  )\n", self->indent, ":"'"${ru_pv_t:+, }${ru_pv_t}"' );
}

pid$target::_ZN2cs3sgw6ussdgw10g_sigAlarmEi:entry,
pid$target::_ZN2cs3sgw6ussdgw10g_sigAlarmEi:return
{
	printf( "%*s'"${ru_pf_t}"'(  )\n", self->indent, ":"'"${ru_pv_t:+, }${ru_pv_t}"' );
}

pid$target::_ZN2cs3sgw6ussdgw14schedule_alarmEj:entry
{
	/*
		arg0: useconds_t nano_to_wait
	*/
	printf( "%*s'"${ru_pf_t}"' | nano_to_wait = %d\n", self->indent, ":"'"${ru_pv_t:+, }${ru_pv_t}"', arg0 );
}

pid$target::_ZN2cs3sgw6ussdgw14schedule_alarmEj:entry
/arg0 == 2000000/
{
	self->tracing = 1;
	self->indent = 1;
	ustack( 99 );
}
'

	echo "${program}" | gawk '{ printf( "%04d:\t\t%s\n", NR, $0 ) }' 1>&2
	echo "${program}" | /usr/sbin/dtrace -64 -q -s /dev/stdin ${target_cmd_set_true:+-c "${target_cmd}"} ${target_pid_set_true:+-p "${target_pid}"}
}

ussdgw_alarm_main "${@}"

exit 0;

__comment='
'

/bin/false <<EOF
EOF
