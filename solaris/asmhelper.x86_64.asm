

function_aaa:                   pushq  %rbp
function_aaa+1:                 movq   %rsp,%rbp
function_aaa+4:                 subq   $0x10,%rsp
function_aaa+8:                 movl   $0x0,%edi
function_aaa+0xd:               call   -0x20a   <PLT:time>
function_aaa+0x12:              movq   %rax,-0x8(%rbp)
function_aaa+0x16:              subq   $0x64,-0x8(%rbp)
function_aaa+0x1b:              movq   -0x8(%rbp),%rax
function_aaa+0x1f:              leave  
function_aaa+0x20:              ret    


function_aab:                   pushq  %rbp
function_aab+1:                 movq   %rsp,%rbp
function_aab+4:                 movl   $0x64,-0x4(%rbp)
function_aab+0xb:               subl   $0xa,-0x4(%rbp)
function_aab+0xf:               movl   -0x4(%rbp),%eax
function_aab+0x12:              popq   %rbp
function_aab+0x13:              ret    


function_aac:                   pushq  %rbp
function_aac+1:                 movq   %rsp,%rbp
function_aac+4:                 movl   %edi,-0x14(%rbp)
function_aac+7:                 movl   %esi,-0x18(%rbp)
function_aac+0xa:               movl   %edx,-0x1c(%rbp)
function_aac+0xd:               movl   -0x18(%rbp),%eax
function_aac+0x10:              movl   -0x14(%rbp),%edx
function_aac+0x13:              addl   %eax,%edx
function_aac+0x15:              movl   -0x1c(%rbp),%eax
function_aac+0x18:              addl   %edx,%eax
function_aac+0x1a:              movl   %eax,-0x4(%rbp)
function_aac+0x1d:              subl   $0xa,-0x4(%rbp)
function_aac+0x21:              movl   -0x4(%rbp),%eax
function_aac+0x24:              popq   %rbp
function_aac+0x25:              ret    


function_aad:                   pushq  %rbp
function_aad+1:                 movq   %rsp,%rbp
function_aad+4:                 movl   %edi,-0x14(%rbp)
function_aad+7:                 movl   %esi,-0x18(%rbp)
function_aad+0xa:               movl   %edx,-0x1c(%rbp)
function_aad+0xd:               movl   %ecx,-0x20(%rbp)
function_aad+0x10:              movl   %r8d,-0x24(%rbp)
function_aad+0x14:              movl   %r9d,-0x28(%rbp)
function_aad+0x18:              movl   -0x18(%rbp),%eax
function_aad+0x1b:              movl   -0x14(%rbp),%edx
function_aad+0x1e:              addl   %eax,%edx
function_aad+0x20:              movl   -0x1c(%rbp),%eax
function_aad+0x23:              addl   %eax,%edx
function_aad+0x25:              movl   -0x20(%rbp),%eax
function_aad+0x28:              addl   %eax,%edx
function_aad+0x2a:              movl   -0x24(%rbp),%eax
function_aad+0x2d:              addl   %eax,%edx
function_aad+0x2f:              movl   -0x28(%rbp),%eax
function_aad+0x32:              addl   %eax,%edx
function_aad+0x34:              movl   0x10(%rbp),%eax
function_aad+0x37:              addl   %eax,%edx
function_aad+0x39:              movl   0x18(%rbp),%eax
function_aad+0x3c:              addl   %eax,%edx
function_aad+0x3e:              movl   0x20(%rbp),%eax
function_aad+0x41:              addl   %edx,%eax
function_aad+0x43:              movl   %eax,-0x4(%rbp)
function_aad+0x46:              subl   $0xa,-0x4(%rbp)
function_aad+0x4a:              movl   -0x4(%rbp),%eax
function_aad+0x4d:              popq   %rbp
function_aad+0x4e:              ret    


function_aae:                   pushq  %rbp
function_aae+1:                 movq   %rsp,%rbp
function_aae+4:                 movq   %rdi,-0x18(%rbp)
function_aae+8:                 movq   -0x18(%rbp),%rax
function_aae+0xc:               movl   (%rax),%eax
function_aae+0xe:               movl   %eax,-0x4(%rbp)
function_aae+0x11:              subl   $0xa,-0x4(%rbp)
function_aae+0x15:              movl   -0x4(%rbp),%eax
function_aae+0x18:              popq   %rbp
function_aae+0x19:              ret    


main:                           pushq  %rbp
main+1:                         movq   %rsp,%rbp
main+4:                         subq   $0x30,%rsp
main+8:                         movl   $-0x1,-0x4(%rbp)
main+0xf:                       movl   $0x0,%eax
main+0x14:                      call   -0xd8    <function_aaa>
main+0x19:                      movl   %eax,-0x4(%rbp)
main+0x1c:                      movl   -0x4(%rbp),%eax
main+0x1f:                      movl   %eax,%esi
main+0x21:                      movl   $0x401100,%edi
main+0x26:                      movl   $0x0,%eax
main+0x2b:                      call   -0x2dc   <PLT:printf>
main+0x30:                      movl   $0x0,%eax
main+0x35:                      call   -0xd8    <function_aab>
main+0x3a:                      movl   %eax,-0x4(%rbp)
main+0x3d:                      movl   -0x4(%rbp),%eax
main+0x40:                      movl   %eax,%esi
main+0x42:                      movl   $0x401100,%edi
main+0x47:                      movl   $0x0,%eax
main+0x4c:                      call   -0x2fd   <PLT:printf>
main+0x51:                      movl   $0x3e8,%edx
main+0x56:                      movl   $0x64,%esi
main+0x5b:                      movl   $0x1,%edi
main+0x60:                      call   -0xef    <function_aac>
main+0x65:                      movl   %eax,-0x4(%rbp)
main+0x68:                      movl   -0x4(%rbp),%eax
main+0x6b:                      movl   %eax,%esi
main+0x6d:                      movl   $0x401100,%edi
main+0x72:                      movl   $0x0,%eax
main+0x77:                      call   -0x328   <PLT:printf>
main+0x7c:                      movl   $0x12,0x10(%rsp)
main+0x84:                      movl   $0x10,0x8(%rsp)
main+0x8c:                      movl   $0xe,(%rsp)
main+0x93:                      movl   $0xc,%r9d
main+0x99:                      movl   $0xa,%r8d
main+0x9f:                      movl   $0x8,%ecx
main+0xa4:                      movl   $0x6,%edx
main+0xa9:                      movl   $0x4,%esi
main+0xae:                      movl   $0x2,%edi
main+0xb3:                      call   -0x11c   <function_aad>
main+0xb8:                      movl   %eax,-0x4(%rbp)
main+0xbb:                      movl   -0x4(%rbp),%eax
main+0xbe:                      movl   %eax,%esi
main+0xc0:                      movl   $0x401100,%edi
main+0xc5:                      movl   $0x0,%eax
main+0xca:                      call   -0x37b   <PLT:printf>
main+0xcf:                      movl   $0x7b,-0x8(%rbp)
main+0xd6:                      leaq   -0x8(%rbp),%rax
main+0xda:                      movq   %rax,%rdi
main+0xdd:                      call   -0xf7    <function_aae>
main+0xe2:                      movl   %eax,-0x4(%rbp)
main+0xe5:                      movl   -0x4(%rbp),%eax
main+0xe8:                      movl   %eax,%esi
main+0xea:                      movl   $0x401100,%edi
main+0xef:                      movl   $0x0,%eax
main+0xf4:                      call   -0x3a5   <PLT:printf>
main+0xf9:                      movl   $0x0,%eax
main+0xfe:                      leave  
main+0xff:                      ret    
