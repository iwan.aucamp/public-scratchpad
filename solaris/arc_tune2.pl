#!/bin/perl
#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
use IPC::Open2;
#use bigint ;
#use bignum;
#use bignum qw/hex oct/;
#use bigint qw/hex oct/;

#use Math::Int64 qw( hex_to_uint64 uint64_to_hex );

use Math::BigInt;

my $opt = {};

Getopt::Long::Configure( "no_ignore_case", "no_auto_abbrev" );
if ( !GetOptions( $opt,
"help|h",
"test|t",
"min|m=s",
"max|M=s"
) )
{
	usage( "GetOptions failed: $SIG{__WARN__}" );
	exit 1;
}

sub usage
{
	my ($error) = @_;
	if ( defined($error) )
	{
		printf("Error : %s\n", $error);
	}
	printf( "%s [--help|-h] [--test|-t] [--min|-m arc_min_bytes] [--max|-M arc_max_bytes]\n", $0 );
}

if ( !defined( $opt->{min} ) && !defined( $opt->{max} ) )
{
	usage( "Must have at least one of --min|-m or --max|-M" );
	exit 1;
}

if ( defined( $opt->{help} ) )
{
	usage();
	exit 0;
}

my $set_c;
my $set_p;

my $testmode = defined( $opt->{test} );
my $arc_min;
my $arc_max;

if ( defined( $opt->{min} ) ) { $arc_min = int( $opt->{min} ); }
if ( defined( $opt->{max} ) ) { $arc_max = int( $opt->{max} ); }

$| = 1;
my $syms = {};
my $values = {};
my $mdb = "/usr/bin/mdb";
open2(*READ, *WRITE, "$mdb -kw") || die "cannot execute mdb";

print( WRITE "arc_stats::print -a arcstat_p.value.ui64 arcstat_c.value.ui64 arcstat_c_max.value.ui64 arcstat_c_min.value.ui64\n" );
print( WRITE "arc_stats/P\n" ); ### Have MDB output paddinf - a line different
				### from the expected ADDR NAME = VAL pattern
while( <READ> ) {
	my $line = $_;

	if ( $line =~ /^ *([a-f0-9]+) (.*\.?.*) =\s*(\S+)\s*$/ ) {
		my $addr = $1;
		my $sym = $2;
		my $value = Math::BigInt->new( $3 );
		print( STDERR "=== FOUND: @ $addr\t= $sym\t= $value ( $3 )\n" );
		$syms->{"$sym"} = $addr;
		$values->{"$sym"} = $value;
	} else { last; }
}
<READ>; ### Buffer the second line of padding output
print( STDERR "=== Done listing vars\n" );

if ( !defined( $syms->{"arcstat_p.value.ui64"} ) || !defined( $syms->{"arcstat_c.value.ui64"} ) || !defined( $syms->{"arcstat_c_max.value.ui64"} ) || !defined( $syms->{"arcstat_c_min.value.ui64"} ) )
{
	die "Could not get all values from kernel ..."
}

if ( !defined( $values->{"arcstat_p.value.ui64"} ) || !defined( $values->{"arcstat_c.value.ui64"} ) || !defined( $values->{"arcstat_c_max.value.ui64"} ) || !defined( $values->{"arcstat_c_min.value.ui64"} ) )
{
	die "Could not get all values from kernel ..."
}

if ( !defined( $arc_max ) )
{
	if ( $arc_min > $values->{"arcstat_c_max.value.ui64"} )
	{
		$arc_max = $arc_min;
		$set_c = $arc_max;
		$set_p = $set_c / 2;
	}
	else
	{
		if ( $arc_min > $values->{"arcstat_c.value.ui64"} ) { $set_c = $arc_min; }
		if ( ( $arc_min / 2 ) > $values->{"arcstat_p.value.ui64"} ) { $set_p = ( $arc_min / 2 ); }
	}
}
else
{
	$set_c = $arc_max;
	$set_p = $set_c / 2;
}

if ( defined( $arc_max ) ) { printf( STDOUT "Requested arc_max: %s bytes = 0x%llx\n", $arc_max, $arc_max ); }
if ( defined( $arc_min ) ) { printf( STDOUT "Requested arc_min: %s bytes = 0x%llx\n", $arc_min, $arc_min ); }
if ( defined( $set_c ) ) { printf( STDOUT "set_c = %s bytes = 0x%llx\n", $set_c, $set_c ); }
if ( defined( $set_p ) ) { printf( STDOUT "set_p = %s bytes = 0x%llx\n", $set_p, $set_p ); }
printf( STDOUT "Test mode: %d\n", $testmode );

printf( STDOUT "Checking ".( $testmode ? "" : "and replacing " )."kernel variables:\n" );
# set c & c_max to our max; set p to max/2
if ( defined( $set_p ) )
{
	printf( STDOUT "p\t @ %s\t= ", $syms->{"arcstat_p.value.ui64"} );
	printf( WRITE "%s/P\n", $syms->{"arcstat_p.value.ui64"} );
	print scalar <READ>;
	if (!$testmode)
	{
		printf( WRITE "%s/Z 0x%llx\n", $syms->{"arcstat_p.value.ui64"}, $set_p );
		print scalar <READ>;
	}
	else
	{
		printf( STDOUT "%s/Z 0x%llx\n", $syms->{"arcstat_p.value.ui64"}, $set_p );
	}
}

if ( defined( $set_c ) )
{
	printf( STDOUT "c\t @ %s\t= ", $syms->{"arcstat_c.value.ui64"} );
	printf( WRITE "%s/P\n", $syms->{"arcstat_c.value.ui64"} );
	print scalar <READ>;
	if (!$testmode)
	{
		printf( WRITE "%s/Z 0x%llx\n", $syms->{"arcstat_c.value.ui64"}, $set_c );
		print scalar <READ>;
	}
	else
	{
		printf( STDOUT "%s/Z 0x%llx\n", $syms->{"arcstat_c.value.ui64"}, $set_c );
	}
}

if ( defined( $arc_max ) )
{
	printf( STDOUT "c_max\t @ %s\t= ", $syms->{"arcstat_c_max.value.ui64"} );
	printf( WRITE "%s/P\n", $syms->{"arcstat_c_max.value.ui64"} );
	print scalar <READ>;
	if (!$testmode)
	{
		printf( WRITE "%s/Z 0x%llx\n", $syms->{"arcstat_c_max.value.ui64"}, $arc_max );
		print scalar <READ>;
	}
	else
	{
		printf( STDOUT "%s/Z 0x%llx\n", $syms->{"arcstat_c_max.value.ui64"}, $arc_max );
	}
}

if ( defined($arc_min) )
{
	printf( STDOUT "c_min\t @ %s\t= ", $syms->{"arcstat_c_min.value.ui64"} );
	printf( WRITE "%s/P\n", $syms->{"arcstat_c_min.value.ui64"} );
	print scalar <READ>;
	if (!$testmode)
	{
		printf( WRITE "%s/Z 0x%llx\n", $syms->{"arcstat_c_min.value.ui64"}, $arc_min );
		print scalar <READ>;
	}
	else
	{
		printf( STDOUT "%s/Z 0x%llx\n", $syms->{"arcstat_c_max.value.ui64"}, $arc_min );
	}
}
