

# void dbuf_rele_and_unlock( dmu_buf_impl_t* arg0, void* arg1 )
# save frame pointer
dbuf_rele_and_unlock:           pushq  %rbp
# set 2nd arg to -1 ... why ? cos 2nd arg of atomic_add_64_nv - decrement ref count
# :: %rsi = -1;
dbuf_rele_and_unlock+1:         movq   $-0x1,%rsi
# set stack pointer as frame pointer
dbuf_rele_and_unlock+8:         movq   %rsp,%rbp
# move stack pointer down 20
dbuf_rele_and_unlock+0xb:       subq   $0x20 ,%rsp
# save callee register
dbuf_rele_and_unlock+0xf:       movq   %r13,-0x8(%rbp)
# save put 1st arg in r13
# :: %r13 = dmu_buf_impl_t* arg0
dbuf_rele_and_unlock+0x13:      movq   %rdi,%r13
# :: %rdi = &arg0->rc_count
dbuf_rele_and_unlock+0x16:      leaq   0x68(%rdi),%rdi
# save callee register
dbuf_rele_and_unlock+0x1a:      movq   %rbx,-0x18(%rbp)
# save callee register
dbuf_rele_and_unlock+0x1e:      movq   %r12,-0x10(%rbp)
# :: %rax = atomic_add_64_nv( %rdi = &arg0->rc_count , %rsi = -1 )
dbuf_rele_and_unlock+0x22:      call   +0x3f9eb9e       <atomic_add_64_nv>
# :: %rdi = arg0->arc_ref_t
dbuf_rele_and_unlock+0x27:      movq   0x78(%r13),%rdi
# keep return value of atomic_add_64_nv in calle save register
# :: %rbx = %rax = atomic_add_64_nv( %rdi = &arg0->rc_count , %rsi = -1 )
dbuf_rele_and_unlock+0x2b:      movq   %rax,%rbx
# jump to dbuf_rele_and_unlock+0xd0 if arg0->arc_ref_t == 0 : 2
dbuf_rele_and_unlock+0x2e:      testq  %rdi,%rdi
dbuf_rele_and_unlock+0x31:      je     +0x9f    <dbuf_rele_and_unlock+0xd0>
# move zero extended arg0->db_level into eax
dbuf_rele_and_unlock+0x37:      movzbl 0x4c(%r13),%eax
# :: if ( arg0->db_level != 0 : 2 ) goto dbuf_rele_and_unlock+0xc6 
dbuf_rele_and_unlock+0x3c:      testb  %al,%al
dbuf_rele_and_unlock+0x3e:      jne    +0x88    <dbuf_rele_and_unlock+0xc6>
# :: %rax = arg0->db_dirtycnt
dbuf_rele_and_unlock+0x44:      movzbq 0xc3(%r13),%rax
# :: %rbx - %rax
dbuf_rele_and_unlock+0x4c:      cmpq   %rbx,%rax
# if ( %rbx = atomic_add_64_nv( %rdi = &arg0->rc_count , %rsi = -1 ) - %rax = arg0->db_dirtycnt == 0 ) goto dbuf_rele_and_unlock+0xcb
dbuf_rele_and_unlock+0x4f:      je     +0x7c    <dbuf_rele_and_unlock+0xcb>
# if ( %rbx = atomic_add_64_nv( %rdi = &arg0->rc_count , %rsi = -1 ) != 0 ) goto dbuf_rele_and_unlock+0x110 +2
dbuf_rele_and_unlock+0x51:      testq  %rbx,%rbx
dbuf_rele_and_unlock+0x54:      jne    +0xbc    <dbuf_rele_and_unlock+0x110>
# :: if ( arg0->db_blkid == DMU_BONUS_BLKID == -1 ) goto dbuf_rele_and_unlock+0x162
dbuf_rele_and_unlock+0x5a:      cmpq   $-0x1,0x40(%r13)
dbuf_rele_and_unlock+0x5f:      je     +0x103   <dbuf_rele_and_unlock+0x162>
# :: %rdi = arg0->arc_ref_t
dbuf_rele_and_unlock+0x65:      movq   0x78(%r13),%rdi
# :: if ( %rdi = arg0->arc_ref_t == 0 ) goto dbuf_rele_and_unlock+0xb0
dbuf_rele_and_unlock+0x69:      testq  %rdi,%rdi
dbuf_rele_and_unlock+0x6c:      je     +0x44    <dbuf_rele_and_unlock+0xb0>
dbuf_rele_and_unlock+0x6e:      nop    
# %rax = arc_ref_anonymous( %rdi = arg0->arc_ref_t )
dbuf_rele_and_unlock+0x70:      call   -0x7b90  <arc_ref_anonymous>
# if ( arc_ref_anonymous( %rdi = arg0->arc_ref_t ) != 0 ) goto dbuf_rele_and_unlock+0x133
dbuf_rele_and_unlock+0x75:      testl  %eax,%eax
dbuf_rele_and_unlock+0x77:      jne    +0xbc    <dbuf_rele_and_unlock+0x133>
# :: %rdi = arg0->arc_ref_t
dbuf_rele_and_unlock+0x7d:      movq   0x78(%r13),%rdi
# :: %rdx = arg0
dbuf_rele_and_unlock+0x81:      movq   %r13,%rdx
# :: %rsi = &dbuf_do_evict
dbuf_rele_and_unlock+0x84:      movq   $0xfffffffff78e71b0,%rsi
# :: arc_inactivate_ref( %rdi = arg0->arc_ref_t, %rsi = &dbuf_do_evict, %rdx = arg0 )
dbuf_rele_and_unlock+0x8b:      call   -0x912b  <arc_inactivate_ref>
# :: %rax = arg->db_objset
dbuf_rele_and_unlock+0x90:      movq   0x20(%r13),%rax
# if ( arg->db_objset->os_primary_cache == 2 == ZFS_CACHE_ALL ) goto dbuf_rele_and_unlock+0x117 
dbuf_rele_and_unlock+0x94:      cmpb   $0x2,0x5e(%rax)
dbuf_rele_and_unlock+0x98:      je     +0x7f    <dbuf_rele_and_unlock+0x117>
# :: %rdi = %r13 = dmu_buf_impl_t* arg0
dbuf_rele_and_unlock+0x9a:      movq   %r13,%rdi
# :: %rax = dbuf_is_metadata( %rdi = dmu_buf_impl_t* arg0 )
dbuf_rele_and_unlock+0x9d:      call   -0x2b7d  <dbuf_is_metadata>
# :: if ( %rax = dbuf_is_metadata( %rdi = dmu_buf_impl_t* arg0 ) == 0 ) goto dbuf_rele_and_unlock+0xb0
dbuf_rele_and_unlock+0xa2:      testl  %eax,%eax
dbuf_rele_and_unlock+0xa4:      je     +0xc     <dbuf_rele_and_unlock+0xb0>
# :: %rax = arg->db_objset
dbuf_rele_and_unlock+0xa6:      movq   0x20(%r13),%rax
# if ( arg->db_objset->os_primary_cache == 1 == ZFS_CACHE_METADATA ) goto dbuf_rele_and_unlock+0x117 
dbuf_rele_and_unlock+0xaa:      cmpb   $0x1,0x5e(%rax)
dbuf_rele_and_unlock+0xae:      je     +0x69    <dbuf_rele_and_unlock+0x117>
# :: %rdi = %r13 = dmu_buf_impl_t* arg0
dbuf_rele_and_unlock+0xb0:      movq   %r13,%rdi
# :: %rax = dbuf_evict( %rdi = dmu_buf_impl_t* arg0 )
dbuf_rele_and_unlock+0xb3:      call   -0xf23   <dbuf_evict>
dbuf_rele_and_unlock+0xb8:      movq   -0x18(%rbp),%rbx
dbuf_rele_and_unlock+0xbc:      movq   -0x10(%rbp),%r12
dbuf_rele_and_unlock+0xc0:      movq   -0x8(%rbp),%r13
dbuf_rele_and_unlock+0xc4:      leave  
dbuf_rele_and_unlock+0xc5:      ret    
dbuf_rele_and_unlock+0xc6:      testq  %rbx,%rbx
dbuf_rele_and_unlock+0xc9:      jne    +0xc     <dbuf_rele_and_unlock+0xd5>
dbuf_rele_and_unlock+0xcb:      call   -0xa47b  <arc_ref_freeze>
dbuf_rele_and_unlock+0xd0:      movzbl 0x4c(%r13),%eax
dbuf_rele_and_unlock+0xd5:      testb  %al,%al
dbuf_rele_and_unlock+0xd7:      jne    -0x86    <dbuf_rele_and_unlock+0x51>
dbuf_rele_and_unlock+0xdd:      movzbq 0xc3(%r13),%rax
dbuf_rele_and_unlock+0xe5:      cmpq   %rbx,%rax
dbuf_rele_and_unlock+0xe8:      jne    -0x97    <dbuf_rele_and_unlock+0x51>
dbuf_rele_and_unlock+0xee:      cmpb   $0x0,0xc0(%r13)
dbuf_rele_and_unlock+0xf6:      je     -0xa5    <dbuf_rele_and_unlock+0x51>
dbuf_rele_and_unlock+0xfc:      movq   %r13,%rdi
dbuf_rele_and_unlock+0xff:      call   -0x2c4f  <dbuf_evict_user>
dbuf_rele_and_unlock+0x104:     jmp    -0xb3    <dbuf_rele_and_unlock+0x51>
dbuf_rele_and_unlock+0x109:     nop    
dbuf_rele_and_unlock+0x10d:     nop    
dbuf_rele_and_unlock+0x110:     cmpq   $0x0,0x70(%r13)
dbuf_rele_and_unlock+0x115:     jne    +0x3c    <dbuf_rele_and_unlock+0x151>
dbuf_rele_and_unlock+0x117:     leaq   0x50(%r13),%rdi
dbuf_rele_and_unlock+0x11b:     nop    
dbuf_rele_and_unlock+0x11e:     nop    
dbuf_rele_and_unlock+0x120:     call   +0x3f60a10       <mutex_exit>
dbuf_rele_and_unlock+0x125:     movq   -0x18(%rbp),%rbx
dbuf_rele_and_unlock+0x129:     movq   -0x10(%rbp),%r12
dbuf_rele_and_unlock+0x12d:     movq   -0x8(%rbp),%r13
dbuf_rele_and_unlock+0x131:     leave  
dbuf_rele_and_unlock+0x132:     ret    
dbuf_rele_and_unlock+0x133:     movq   0x78(%r13),%rdi
dbuf_rele_and_unlock+0x137:     call   -0x9307  <arc_free_ref>
dbuf_rele_and_unlock+0x13c:     movq   $0x0,0x78(%r13)
dbuf_rele_and_unlock+0x144:     movq   %r13,%rdi
dbuf_rele_and_unlock+0x147:     call   -0xfb7   <dbuf_evict>
dbuf_rele_and_unlock+0x14c:     jmp    -0x94    <dbuf_rele_and_unlock+0xb8>
dbuf_rele_and_unlock+0x151:     leaq   0x80(%r13),%rdi
dbuf_rele_and_unlock+0x158:     call   +0x40d2b28       <cv_broadcast>
dbuf_rele_and_unlock+0x15d:     nop    
dbuf_rele_and_unlock+0x160:     jmp    -0x49    <dbuf_rele_and_unlock+0x117>
dbuf_rele_and_unlock+0x162:     leaq   0x50(%r13),%rdi
dbuf_rele_and_unlock+0x166:     call   +0x3f609ca       <mutex_exit>
dbuf_rele_and_unlock+0x16b:     movq   0x28(%r13),%rdi
dbuf_rele_and_unlock+0x16f:     movl   $0x1,%esi
dbuf_rele_and_unlock+0x174:     call   +0x3f609ec       <rw_enter>
dbuf_rele_and_unlock+0x179:     movq   0x28(%r13),%rax
dbuf_rele_and_unlock+0x17d:     movq   0x8(%rax),%rbx
dbuf_rele_and_unlock+0x181:     leaq   0x238(%rbx),%r12
dbuf_rele_and_unlock+0x188:     movq   %r12,%rdi
dbuf_rele_and_unlock+0x18b:     call   +0x3f60885       <mutex_enter>
dbuf_rele_and_unlock+0x190:     decl   0x94(%rbx)
dbuf_rele_and_unlock+0x196:     movq   %r12,%rdi
dbuf_rele_and_unlock+0x199:     call   +0x3f60997       <mutex_exit>
dbuf_rele_and_unlock+0x19e:     movq   0x28(%r13),%rdi
dbuf_rele_and_unlock+0x1a2:     call   +0x3f60a3e       <rw_exit>
dbuf_rele_and_unlock+0x1a7:     movq   0x28(%r13),%rax
dbuf_rele_and_unlock+0x1ab:     movq   %r13,%rsi
dbuf_rele_and_unlock+0x1ae:     movq   0x8(%rax),%rdi
dbuf_rele_and_unlock+0x1b2:     call   +0x1273e <dnode_rele>
dbuf_rele_and_unlock+0x1b7:     jmp    -0x92    <dbuf_rele_and_unlock+0x125>


# int arc_ref_anonymous( arc_ref_t* )
arc_ref_anonymous:              pushq  %rbp
arc_ref_anonymous+1:            movl   $0x1,%esi
arc_ref_anonymous+6:            movq   %rsp,%rbp
arc_ref_anonymous+9:            subq   $0x20,%rsp
arc_ref_anonymous+0xd:          movq   %r12,-0x10(%rbp)
arc_ref_anonymous+0x11:         leaq   0x38(%rdi),%r12
arc_ref_anonymous+0x15:         movq   %rbx,-0x18(%rbp)
arc_ref_anonymous+0x19:         movq   %r13,-0x8(%rbp)
arc_ref_anonymous+0x1d:         movq   %rdi,%rbx
arc_ref_anonymous+0x20:         xorl   %r13d,%r13d
arc_ref_anonymous+0x23:         movq   %r12,%rdi
arc_ref_anonymous+0x26:         call   +0x3f6865a       <rw_enter>
arc_ref_anonymous+0x2b:         movq   0x20(%rbx),%rax
arc_ref_anonymous+0x2f:         cmpq   $0xfffffffffbcfe4a0,%rax
arc_ref_anonymous+0x35:         je     +0x23    <arc_ref_anonymous+0x58>
arc_ref_anonymous+0x37:         cmpq   $0xfffffffffbcfe400,%rax
arc_ref_anonymous+0x3d:         je     +0x1b    <arc_ref_anonymous+0x58>
arc_ref_anonymous+0x3f:         movq   %r12,%rdi
arc_ref_anonymous+0x42:         call   +0x3f686be       <rw_exit>
arc_ref_anonymous+0x47:         movl   %r13d,%eax
arc_ref_anonymous+0x4a:         movq   -0x18(%rbp),%rbx
arc_ref_anonymous+0x4e:         movq   -0x10(%rbp),%r12
arc_ref_anonymous+0x52:         movq   -0x8(%rbp),%r13
arc_ref_anonymous+0x56:         leave  
arc_ref_anonymous+0x57:         ret    
arc_ref_anonymous+0x58:         movl   $0x1,%r13d
arc_ref_anonymous+0x5e:         movq   %r12,%rdi
arc_ref_anonymous+0x61:         call   +0x3f6869f       <rw_exit>
arc_ref_anonymous+0x66:         movl   %r13d,%eax
arc_ref_anonymous+0x69:         movq   -0x18(%rbp),%rbx
arc_ref_anonymous+0x6d:         movq   -0x10(%rbp),%r12
arc_ref_anonymous+0x71:         movq   -0x8(%rbp),%r13
arc_ref_anonymous+0x75:         leave  
arc_ref_anonymous+0x76:         ret    

# void rw_enter( krwlock_t *rwlp, krw_t rw )
# void rw_exit( krwlock_t *rwlp )


# void arc_inactivate_ref( arc_ref_t *, arc_evict_func_t *, void * )
arc_inactivate_ref:             pushq  %rbp
arc_inactivate_ref+1:           movq   %rsp,%rbp
arc_inactivate_ref+4:           subq   $0x30,%rsp
arc_inactivate_ref+8:           movq   %r15,-0x8(%rbp)
arc_inactivate_ref+0xc:         leaq   0x38(%rdi),%r15
arc_inactivate_ref+0x10:        movq   %rbx,-0x28(%rbp)
arc_inactivate_ref+0x14:        movq   %r12,-0x20(%rbp)
arc_inactivate_ref+0x18:        movq   %rsi,%rbx
arc_inactivate_ref+0x1b:        movq   %rdi,%r12
arc_inactivate_ref+0x1e:        xorl   %esi,%esi
arc_inactivate_ref+0x20:        movq   %r15,%rdi
arc_inactivate_ref+0x23:        movq   %r13,-0x18(%rbp)
arc_inactivate_ref+0x27:        movq   %r14,-0x10(%rbp)
arc_inactivate_ref+0x2b:        movq   %rdx,%r14
arc_inactivate_ref+0x2e:        call   +0x3f69bd2       <rw_enter>
arc_inactivate_ref+0x33:        movq   0x20(%r12),%r13
arc_inactivate_ref+0x38:        movq   0x8(%r12),%rdi
arc_inactivate_ref+0x3d:        cmpq   0x60(%r13),%rdi
arc_inactivate_ref+0x41:        je     +0x12    <arc_inactivate_ref+0x53>
arc_inactivate_ref+0x43:        movl   0x20(%r13),%edx
arc_inactivate_ref+0x47:        movq   (%r12),%rsi
arc_inactivate_ref+0x4b:        andl   $0x1,%edx
arc_inactivate_ref+0x4e:        call   -0x40e   <arc_free_data_block>
arc_inactivate_ref+0x53:        movq   %rbx,0x10(%r12)
arc_inactivate_ref+0x58:        leaq   0x90(%r13),%rbx
arc_inactivate_ref+0x5f:        movq   %r14,0x18(%r12)
arc_inactivate_ref+0x64:        movq   $0x0,0x8(%r12)
arc_inactivate_ref+0x6d:        movq   %rbx,%rdi
arc_inactivate_ref+0x70:        call   +0x3f69a40       <mutex_enter>
arc_inactivate_ref+0x75:        leaq   0x40(%r13),%rdi
arc_inactivate_ref+0x79:        movq   %r12,%rsi
arc_inactivate_ref+0x7c:        call   +0x4141974       <list_insert_head>
arc_inactivate_ref+0x81:        movq   %rbx,%rdi
arc_inactivate_ref+0x84:        call   +0x3f69b4c       <mutex_exit>
arc_inactivate_ref+0x89:        movq   %r12,%rsi
arc_inactivate_ref+0x8c:        movq   %r13,%rdi
arc_inactivate_ref+0x8f:        call   -0x100f  <arc_rele>
arc_inactivate_ref+0x94:        movq   %r15,%rdi
arc_inactivate_ref+0x97:        call   +0x3f69be9       <rw_exit>
arc_inactivate_ref+0x9c:        movq   -0x28(%rbp),%rbx
arc_inactivate_ref+0xa0:        movq   -0x20(%rbp),%r12
arc_inactivate_ref+0xa4:        movq   -0x18(%rbp),%r13
arc_inactivate_ref+0xa8:        movq   -0x10(%rbp),%r14
arc_inactivate_ref+0xac:        movq   -0x8(%rbp),%r15
arc_inactivate_ref+0xb0:        leave  
arc_inactivate_ref+0xb1:        ret    


# boolean_t dbuf_is_metadata( dmu_buf_impl_t* )
dbuf_is_metadata:               pushq  %rbp
dbuf_is_metadata+1:             movq   %rsp,%rbp
dbuf_is_metadata+4:             subq   $0x10,%rsp
dbuf_is_metadata+8:             movq   %rbx,(%rsp)
dbuf_is_metadata+0xc:           movq   %r12,0x8(%rsp)
dbuf_is_metadata+0x11:          movq   %rdi,%rbx
dbuf_is_metadata+0x14:          cmpb   $0x0,0x4c(%rdi)
dbuf_is_metadata+0x18:          movl   $0x1,%r12d
dbuf_is_metadata+0x1e:          jne    +0x33    <dbuf_is_metadata+0x51>
dbuf_is_metadata+0x20:          cmpq   $-0x2,0x40(%rdi)
dbuf_is_metadata+0x25:          je     +0x2c    <dbuf_is_metadata+0x51>
dbuf_is_metadata+0x27:          movq   0x28(%rdi),%rdi
dbuf_is_metadata+0x2b:          movl   $0x1,%esi
dbuf_is_metadata+0x30:          call   +0x3f63610       <rw_enter>
dbuf_is_metadata+0x35:          movq   0x28(%rbx),%rdi
dbuf_is_metadata+0x39:          movq   0x8(%rdi),%rax
dbuf_is_metadata+0x3d:          movl   0x48(%rax),%eax
dbuf_is_metadata+0x40:          leaq   (%rax,%rax,2),%rax
dbuf_is_metadata+0x44:          movl   0xf798cb28(,%rax,8),%r12d
dbuf_is_metadata+0x4c:          call   +0x3f63674       <rw_exit>
dbuf_is_metadata+0x51:          movl   %r12d,%eax
dbuf_is_metadata+0x54:          movq   (%rsp),%rbx
dbuf_is_metadata+0x58:          movq   0x8(%rsp),%r12
dbuf_is_metadata+0x5d:          leave  
dbuf_is_metadata+0x5e:          ret    


# void dbuf_evict( dmu_buf_impl_t * )
dbuf_evict:                     pushq  %rbp
dbuf_evict+1:                   movq   %rsp,%rbp
dbuf_evict+4:                   pushq  %r13
dbuf_evict+6:                   xorl   %r13d,%r13d
dbuf_evict+9:                   pushq  %r12
dbuf_evict+0xb:                 xorl   %r12d,%r12d
dbuf_evict+0xe:                 pushq  %rbx
dbuf_evict+0xf:                 movq   %rdi,%rbx
dbuf_evict+0x12:                subq   $0x8,%rsp
dbuf_evict+0x16:                call   -0x1cf6  <dbuf_evict_user>
dbuf_evict+0x1b:                movq   0x28(%rbx),%rdi
dbuf_evict+0x1f:                movl   $0x1,%esi
dbuf_evict+0x24:                call   +0x3f619ac       <rw_enter>
dbuf_evict+0x29:                movq   0x28(%rbx),%rdi
dbuf_evict+0x2d:                movq   0x30(%rbx),%rax
dbuf_evict+0x31:                movq   0x8(%rdi),%rdx
dbuf_evict+0x35:                cmpq   0x28(%rdx),%rax
dbuf_evict+0x39:                cmovq.ne %rax,%r12
dbuf_evict+0x3d:                call   +0x3f61a13       <rw_exit>
dbuf_evict+0x42:                cmpq   $-0x1,0x40(%rbx)
dbuf_evict+0x47:                je     +0x7e    <dbuf_evict+0xc5>
dbuf_evict+0x49:                leaq   0x98(%rbx),%rdi
dbuf_evict+0x50:                call   +0x4139960       <list_link_active>
dbuf_evict+0x55:                testl  %eax,%eax
dbuf_evict+0x57:                je     +0x5e    <dbuf_evict+0xb5>
dbuf_evict+0x59:                movq   0x78(%rbx),%rdi
dbuf_evict+0x5d:                movq   $0x0,0x18(%rbx)
dbuf_evict+0x65:                movl   $0x5,0x60(%rbx)
dbuf_evict+0x6c:                movq   $0x0,0x30(%rbx)
dbuf_evict+0x74:                testq  %rdi,%rdi
dbuf_evict+0x77:                jne    +0x76    <dbuf_evict+0xed>
dbuf_evict+0x79:                nop    
dbuf_evict+0x7d:                nop    
dbuf_evict+0x80:                leaq   0x50(%rbx),%rdi
dbuf_evict+0x84:                call   +0x3f6191c       <mutex_exit>
dbuf_evict+0x89:                testq  %r12,%r12
dbuf_evict+0x8c:                je     +0xd     <dbuf_evict+0x99>
dbuf_evict+0x8e:                movq   %rbx,%rsi
dbuf_evict+0x91:                movq   %r12,%rdi
dbuf_evict+0x94:                call   +0xd9c   <dbuf_rele>
dbuf_evict+0x99:                cmpl   $0x96,%r13d
dbuf_evict+0xa0:                je     +0xa     <dbuf_evict+0xaa>
dbuf_evict+0xa2:                movq   %rbx,%rdi
dbuf_evict+0xa5:                call   +0x82b   <dbuf_destroy>
dbuf_evict+0xaa:                addq   $0x8,%rsp
dbuf_evict+0xae:                popq   %rbx
dbuf_evict+0xaf:                popq   %r12
dbuf_evict+0xb1:                popq   %r13
dbuf_evict+0xb3:                leave  
dbuf_evict+0xb4:                ret    
dbuf_evict+0xb5:                movq   0x28(%rbx),%rax
dbuf_evict+0xb9:                movq   %rbx,%rsi
dbuf_evict+0xbc:                movq   0x8(%rax),%rdi
dbuf_evict+0xc0:                call   +0x136a0 <dnode_rele>
dbuf_evict+0xc5:                movq   0x78(%rbx),%rdi
dbuf_evict+0xc9:                movq   $0x0,0x28(%rbx)
dbuf_evict+0xd1:                movq   $0x0,0x18(%rbx)
dbuf_evict+0xd9:                movl   $0x5,0x60(%rbx)
dbuf_evict+0xe0:                movq   $0x0,0x30(%rbx)
dbuf_evict+0xe8:                testq  %rdi,%rdi
dbuf_evict+0xeb:                je     -0x6b    <dbuf_evict+0x80>
dbuf_evict+0xed:                call   -0x732d  <arc_evict_ref>
dbuf_evict+0xf2:                testl  %eax,%eax
dbuf_evict+0xf4:                movl   %eax,%r13d
dbuf_evict+0xf7:                jne    -0x77    <dbuf_evict+0x80>
dbuf_evict+0xf9:                movq   $0x0,0x78(%rbx)
dbuf_evict+0x101:               jmp    -0x81    <dbuf_evict+0x80>


# void dbuf_evict_user( dmu_buf_impl_t * )
dbuf_evict_user:                pushq  %rbp
dbuf_evict_user+1:              movq   %rsp,%rbp
dbuf_evict_user+4:              pushq  %rbx
dbuf_evict_user+5:              movq   %rdi,%rbx
dbuf_evict_user+8:              subq   $0x8,%rsp
dbuf_evict_user+0xc:            cmpb   $0x0,0x4c(%rdi)
dbuf_evict_user+0x10:           jne    +0x55    <dbuf_evict_user+0x65>
dbuf_evict_user+0x12:           movq   0xb8(%rdi),%rax
dbuf_evict_user+0x19:           testq  %rax,%rax
dbuf_evict_user+0x1c:           je     +0x49    <dbuf_evict_user+0x65>
dbuf_evict_user+0x1e:           movq   0xb0(%rdi),%rdx
dbuf_evict_user+0x25:           testq  %rdx,%rdx
dbuf_evict_user+0x28:           je     +0x10    <dbuf_evict_user+0x38>
dbuf_evict_user+0x2a:           movq   0x18(%rdi),%rax
dbuf_evict_user+0x2e:           movq   %rax,(%rdx)
dbuf_evict_user+0x31:           movq   0xb8(%rdi),%rax
dbuf_evict_user+0x38:           movq   0xa8(%rbx),%rsi
dbuf_evict_user+0x3f:           movq   %rbx,%rdi
dbuf_evict_user+0x42:           call   *%rax
dbuf_evict_user+0x44:           movq   $0x0,0xa8(%rbx)
dbuf_evict_user+0x4f:           movq   $0x0,0xb0(%rbx)
dbuf_evict_user+0x5a:           movq   $0x0,0xb8(%rbx)
dbuf_evict_user+0x65:           addq   $0x8,%rsp
dbuf_evict_user+0x69:           popq   %rbx
dbuf_evict_user+0x6a:           leave  
dbuf_evict_user+0x6b:           ret    

# uint64_t atomic_add_64_nv( volatile uint64_t* target, int64_t delta )
atomic_add_64_nv:               movq   %rsi,%rax
atomic_add_64_nv+3:             lock xaddq %rsi,(%rdi)
atomic_add_64_nv+8:             addq   %rsi,%rax
atomic_add_64_nv+0xb:           ret    

