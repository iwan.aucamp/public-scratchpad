#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

int main(int argc,char **argv)
{
	assert( argc == 2 );

	size_t size = 0;
	fprintf( stderr, "size (str) = \"%s\"\n", argv[1] );
	char *endptr = NULL;
	errno = 0;
	long long int llsize = strtoll( argv[1], &endptr, 10 );
	fprintf( stderr, "size (ll) = %lld\n", llsize );
	fprintf( stderr, "size (ep) = \"%s\"\n", endptr );
	fprintf( stderr, "size (ep) = %p\n", endptr );
	fprintf( stderr, "size (ep) = %p\n", argv[1] + strlen( argv[1] ) );
	assert( *endptr == '\0' );
	assert( endptr == argv[1] + strlen( argv[1] ) );
	assert( errno == 0 );
	size = (size_t)llsize;
	//strtoull(  );
	fprintf( stderr, "size = %zd\n", size );
	
	fprintf( stderr, "mmap( %p, %zd, %i, %i, %i, %zd )\n", (void*)NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, (size_t)0 );
	void* addr = mmap( (void*)NULL, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, (size_t)0 );
	assert( addr );
	fprintf( stderr, "addr = %p\n", addr );

	for ( size_t i = 0; i < size/4096; ++i )
	{
		*((char*)( addr + (i * 4096))) = 0;
	}

	munmap( addr, size );
	return EXIT_SUCCESS;
}
