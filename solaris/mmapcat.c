#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

int main(int argc,char **argv)
{
	assert( argc >= 2 || argc <= 4 );
	fprintf( stderr, "filename = %s\n", argv[1] );
	int file = open( argv[1], O_RDONLY );
	assert( file > 0 );

	size_t size = 0;
	if ( argc >= 3 )
	{
		fprintf( stderr, "size (str) = \"%s\"\n", argv[2] );
		char *endptr = NULL;
		errno = 0;
		long long int llsize = strtoll( argv[2], &endptr, 10 );
		fprintf( stderr, "size (ll) = %lld\n", llsize );
		fprintf( stderr, "size (ep) = \"%s\"\n", endptr );
		fprintf( stderr, "size (ep) = %p\n", endptr );
		fprintf( stderr, "size (ep) = %p\n", argv[2] + strlen( argv[2] ) );
		assert( *endptr == '\0' );
		assert( endptr == argv[2] + strlen( argv[2] ) );
		assert( errno == 0 );
		size = (size_t)llsize;
		//strtoull(  );
		fprintf( stderr, "size = %zd\n", size );
	}

	size_t offset = 0;
	if ( argc >= 4 )
	{
		char *endptr = NULL;
		errno = 0;
		offset = (size_t)strtoll( argv[3], &endptr, 10 );
		assert( *endptr == '\0' );
		assert( endptr == argv[3] + strlen( argv[3] ) );
		assert( errno == 0 );
		fprintf( stderr, "offset = %zd\n", offset );
	}


	struct stat st;
	fstat( file, &st );

	fprintf( stderr, "st.st_size = %zd\n", st.st_size );

	fprintf( stderr, "mmap( %p, %zd, %i, %i, %i, %zd )\n", (void*)NULL, ( st.st_size != 0 ? (size_t)st.st_size : size ), PROT_READ, MAP_PRIVATE, file, (size_t)0 );
	void* addr = mmap( NULL, st.st_size, PROT_READ, MAP_PRIVATE, file, 0 );
	assert( addr );
	fprintf( stderr, "addr = %p\n", addr );

	fprintf( stderr, "st.st_size = %zd\n", st.st_size );

	fprintf( stderr, "fwrite( %p, %zd, %zd, %p )\n", addr + offset, ( size != 0 ? size : (size_t)st.st_size ), (size_t)1, stdout );
	fwrite( addr + offset, ( size != 0 ? size : (size_t)st.st_size ), 1, stdout );

	munmap( addr, st.st_size );
	close( file );
	return EXIT_SUCCESS;
}
