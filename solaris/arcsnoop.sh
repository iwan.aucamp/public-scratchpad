#!/bin/bash

script_basename=$(basename "${0}")
script_dirname=$(dirname "${0}")

expand_boolean()
{
	local boolean_name=${1}
	local boolean_value
	eval "boolean_value=\${${boolean_name}}"
	if "${boolean_value}"
	then
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

expand_test()
{
	local boolean_name=${1}
	shift 1
	if "${@}"
	then
		eval "${boolean_name}=true"
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}=false"
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

do_if_true()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		"${@}"
		return "${?}"
	else
		return 1
	fi
}

do_if_false()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		return 1
	else
		"${@}"
		return "${?}"
	fi
}

verbosity_to_flags()
{
	local flag="${1}"
	local remainder="${verbosity:--1}"
	while [ "${remainder}" -gt "-1" ]
	do
		remainder=$(( ${remainder} - 1 ))
		echo -n " ${flag}"
	done
	echo
}

verbose_echo()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
	echo "${@}" 1>&2
	return 0
}

verbose_dump_vars()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_vars_msg()
{
	local level="${1}"
	local msg="${2}"
	shift 2
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ ${msg} " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_functions()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for function in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -f "${function}" 1>&2
	done
	return 0
}

error_echo()
{
	echo "${@}" 1>&2
}

arcsnoop_main_usage()
{
	stty tabs
	test "${#}" -ge 1 && { error_echo "ERROR: ${@}"; }
	local align="\r\t\t\t"
	error_echo -e "${script_basename} [options] [interval] [count]"
	error_echo -e "options:"
	error_echo -e " -P predicate${align}predicate to use when tracing"
	error_echo -e " -v${align}increase verbosity"
	error_echo -e " -d${align}pretend mode"
	error_echo -e " -u depth${align}user stack depth"
	error_echo -e " -k depth${align}kernel stack depth"
	error_echo -e " -f (all|data|meta)${align}data filter"
	error_echo -e " -l (all|hit|miss)${align}location filter"
	error_echo -e " -t${align}trace"
	error_echo -e " -s${align}speculative tracing"
	error_echo -e " -S${align}speculative tracing + debug"
	error_echo -e " -w seconds${align}trace duration"
	error_echo -e " -E${align}trace evicts"
	error_echo -e " -R${align}trace reads"
	error_echo -e " -I${align}enable probesets without predicates"
	error_echo -e " -i${align}enable probesets with predicates"
	return 0;
}

expand_probesets()
{
	local probesets=( "${@}" )
	local probes=""
	for probeset_name in "${probesets[@]}"
	do
		local probeset=""
		eval 'probeset="${probeset_'${probeset_name}'}"'
		verbose_dump_vars 1 probeset
		test -n "${probeset}" || { error_echo "Invalid probeset name ${probeset_name}"; return 1;}
		probes="${probes:+${probes}$',\n'}${probeset}"
	done
	echo "${probes}"
	return 0
}

arcsnoop_main()
{
	local argv=( "${@}" )
	local verbosity=-1
	local verbose=false
	local pretend=false

	local user_stack=0
	local kernel_stack=0
	local trace=false
	local filter="all" # all|data|meta
	local loc="all" # all|hit|miss|undef
	local speculate=false
	local speculate_debug=false
	local speculate_nspec=""
	local duration=false
	local duration_seconds=""
	local output_file=""
	local user_predicate="1"
	local debug=false

	local -a probesets_pri
	local -a probesets_prx

	verbose_echo 1 "${FUNCNAME}: @=${@}"
	local OPTARG OPTERR option OPTIND
	while getopts "hvdP:u:k:tf:l:sSc:w:o:Di:I:" option "${@}"
	do
		case "${option}" in
			h)
				"${FUNCNAME}_usage"
				return 0;
				;;
			v)
				verbosity=$(( ${verbosity} + 1 ))
				;;
			d)
				pretend=true;
				;;
			P)
				user_predicate="${OPTARG}"
				;;
			u)
				user_stack="${OPTARG}"
				;;
			k)
				kernel_stack="${OPTARG}"
				;;
			t)
				trace=true
				;;
			f)
				filter="${OPTARG}"
				;;
			l)
				loc="${OPTARG}"
				;;
			s)
				speculate=true
				;;
			S)
				speculate_debug=true
				;;
			c)
				speculate_nspec="${OPTARG}"
				;;
			w)
				duration=true
				duration_seconds="${OPTARG}"
				;;
			o)
				output_file="${OPTARG}"
				;;
			D)
				debug=true
				;;
			i)
				probesets_pri=( "${probesets_pri[@]}" ${OPTARG} )
				;;
			I)
				probesets_prx=( "${probesets_prx[@]}" ${OPTARG} )
				;;
			*)
				"${FUNCNAME}_usage" "Invalid options [ OPTARG=${OPTARG}, OPTERR=${OPTERR}, option=${option} ]"
				exit 1
				;;
		esac
	done
	test "${verbosity}" -ge 0 && { verbose=true; }
	verbose_dump_vars 1 script_dirname script_basename argv OPTERR option OPTIND
	verbose_dump_vars 1 verbosity pretend user_stack kernel_stack
	verbose_dump_vars 1 trace filter loc speculate speculate_debug speculate_nspec duration duration_seconds
	verbose_dump_vars 1 output_file probesets_pri probesets_prx
	shift $(( ${OPTIND} - 1 ))

	local pretend_true pretend_false
	expand_boolean pretend

	local user_stack_set_true user_stack_set_false
	expand_test user_stack_set test "${user_stack}" -ne 0
	verbose_dump_vars 1 user_stack_set_true user_stack_set_false

	local kernel_stack_set_true kernel_stack_set_false
	expand_test kernel_stack_set test "${kernel_stack}" -ne 0
	verbose_dump_vars 1 kernel_stack_set_true kernel_stack_set_false

	local trace_true trace_false
	expand_boolean trace

	local speculate_true speculate_false
	expand_boolean speculate

	local speculate_debug_true speculate_debug_false
	expand_boolean speculate_debug

	local duration_true duration_false
	expand_boolean duration

	local output_file_set_true output_file_set_false
	expand_test output_file_set test -n "${output_file}"

	local debug_true debug_false
	expand_boolean debug

	"${speculate}" && ! "${trace}" && {
		${FUNCNAME}_usage "-s needs -t" ; return 1;
	}

	"${speculate_debug}" && ! "${speculate}" && {
		${FUNCNAME}_usage "-S needs -s" ; return 1;
	}

	"${speculate}" && test -z "${speculate_nspec}" && {
		local nspec=$( psrinfo | gawk 'BEGIN{ count=0; } { count=count+1; } END{ print ( count + 1 ); }' )
		verbose_dump_vars 1 nspec
	}

	"${speculate}" && test -n "${speculate_nspec}" && {
		local nspec="${speculate_nspec}"
		verbose_dump_vars 1 nspec
	}


	test "${user_stack}" -lt 0 && user_stack=""
	local user_stack_part=""
	"${user_stack_set}" && {
		user_stack_part=", ustack( ${user_stack} )"
	}

	test "${kernel_stack}" -lt 0 && kernel_stack=""
	local kernel_stack_part=""
	"${kernel_stack_set}" && {
		kernel_stack_part=", stack( ${kernel_stack} )"
	}


	verbose_dump_vars 1 loc
	local reactivate_loc_predicate=""
	local read_loc_predicate=""
	case "${loc}" in
		hit)
			reactivate_loc_predicate="( self->reactivate_hit == 2 )";
			read_loc_predicate="( self->read_hit == 2 )";
			;;
		miss)
			reactivate_loc_predicate="( self->reactivate_hit == 1 )";
			read_loc_predicate="( self->read_hit == 1 )";
			;;
		undef)
			reactivate_loc_predicate="( self->reactivate_hit == -1 )";
			read_loc_predicate="( self->read_hit == -1 )";
			;;
		all)
			reactivate_loc_predicate="( 1 )";
			read_loc_predicate="( 1 )";
			;;
		*)
			"${FUNCNAME}_usage" "invalid value for loc: (all|hit|miss|undef)"
			exit 1
			;;
	esac

	verbose_dump_vars 1 filter
	local reactivate_filter_predicate=""
	local read_filter_predicate=""
	case "${filter}" in
		meta)
			reactivate_filter_predicate="( ( self->reactivate_arc_flags & 1 ) == 1 )";
			read_filter_predicate="( ( self->read_arc_flags & 1 ) == 1 )";
			;;
		data)
			reactivate_filter_predicate="( ( self->reactivate_arc_flags & 1 ) == 0 )";
			read_filter_predicate="( ( self->read_arc_flags & 1 ) == 0 )";
			;;
		all)
			reactivate_filter_predicate="( 1 )";
			read_filter_predicate="( 1 )";
			;;
		*)
			"${FUNCNAME}_usage" "invalid value for filter: (all|meta|data)"
			exit 1
			;;
	esac

	predicate="${user_predicate}"
	#test -n "${execname}" && predicate="${predicate}${predicate:+ && }execname == \"${execname}\""
	#test -n "${pid}" && predicate="${predicate}${predicate:+ && }pid == ${pid}"

	predicate="( ${predicate} )"
	verbose_dump_vars 1 predicate

	ru_pf_t="( %i ):( %s:%s:%s:%s )( %s:%i:%i ):( indent=%i, speculation=%i, tracing=%i ):"
	ru_pv_t="walltimestamp, probeprov, probemod, probefunc, probename, execname, pid, tid, self->indent, self->speculation, self->tracing"

	probeset_evict='fbt:zfs:arc_add_to_evictables:<ername>,
fbt:zfs:arc_evict_from_ghost:<ername>, fbt:zfs:arc_evict_spa_from_ghost:<ername>,
fbt:zfs:arc_evict_buf:<ername>, fbt:zfs:arc_evict_bytes:<ername>,
fbt:zfs:arc_evict_spa:<ername>, fbt:zfs:arc_evict_ref:<ername>,
fbt:zfs:arc_do_user_evicts:<ername>,
fbt:zfs:dbuf_evict_user:<ername>, fbt:zfs:dbuf_evict:<ername>'
	probeset_read='syscall::read:<ername>, syscall::readv:<ername>, syscall::pread:<ername>, syscall::pread64:<ername>,
fbt:genunix:read:<ername>, fbt:genunix:pread:<ername>, fbt:genunix:readv:<ername>,
fbt:genunix:pread32:<ername>, fbt:genunix:read32:<ername>, fbt:genunix:pread64:<ername>, fbt:genunix:readv32:<ername>,
fbt:zfs:zfs_read:<ername>, fbt:zfs:zfs_shim_read:<ername>'
	probeset_getpage='fbt:zfs:zfs_getpage:<ername>, fbt:zfs:zfs_shim_getpage:<ername>,
fbt:genunix:fop_getpage:<ername>'
	probeset_arc='fbt:zfs:arc_read:<ername>,
fbt:zfs:arc_reactivate_ref:<ername>'

	local pri_probes pri_entry pri_inside pri_return
	test "${#probesets_pri[@]}" -gt 0 && {
		pri_probes=$( expand_probesets "${probesets_pri[@]}" ) || { error_echo "Failed to expand probesets probesets_pri=( ${probesets_pri[@]} )"; return 1; }
		verbose_dump_vars 1 pri_probes

		pri_entry='
/*
'${debug_true:+ "${pri_probes//<ername>/entry}"'
{
	printf( "'"${ru_pf_t}"'( execname = %s )\n",
		'"${ru_pv_t}"', execname );
	@stacks[ probeprov, probemod, probefunc, probename, execname, ustack(), stack() ] = count();
}'
}'
*/

'"${pri_probes//<ername>/entry}"'
/opt_speculate && '"${duration_true:+duration >= 0 && }${predicate}"' && !self->tracing && !self->speculation/
{
	self->speculation = speculation();
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->speculation = ... )\n",
		'"${ru_pv_t}"' );'}'
}

'"${pri_probes//<ername>/entry}"'
/'"${speculate_false:+${duration_true:+duration >= 0 && }${predicate}}${speculate_true:+self->speculation}"'/
{
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	self->tracing = self->tracing + 1;
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
}

'"${pri_probes//<ername>/entry}"'
/opt_speculate_debug && self->tracing/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( speculate( self->speculation ) )\n", self->indent, "{",
		'"${ru_pv_t}"' ) : 0;
}
'
	pri_inside='
'

	pri_return='
'"${pri_probes//<ername>/return}"'
/opt_speculate && self->tracing == 1/
{
	'"${speculate_true:+commit( self->speculation );}"'
	self->speculation = 0;
}

'"${pri_probes//<ername>/return}"'
/opt_speculate_debug && self->tracing == 1/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( commit( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

'"${pri_probes//<ername>/return}"'
/self->tracing/
{
	self->tracing = self->tracing - 1;
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
}
'
	}

	local prx_probes prx_entry prx_inside prx_return
	test "${#probesets_prx[@]}" -gt 0 && {
		prx_probes=$( expand_probesets "${probesets_prx[@]}" ) || { error_echo "Failed to expand probesets probesets_prx=( ${probesets_prx[@]} )"; return 1; }
		verbose_dump_vars 1 prx_probes

		prx_entry='
'"${prx_probes//<ername>/entry}"'
/opt_speculate && '"${duration_true:+duration >= 0 && }"'!self->tracing && !self->speculation/
{
	self->speculation = speculation();
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->speculation = ... )\n",
		'"${ru_pv_t}"' );'}'
}

'"${prx_probes//<ername>/entry}"'
/'"${speculate_false:+${duration_true:+duration >= 0 && }}${speculate_true:+self->speculation && }"'1/
{
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	self->tracing = self->tracing + 1;
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
	/*@stacks[ probeprov, probemod, probefunc, probename, execname, ustack(), stack() ] = count();*/
}

'"${prx_probes//<ername>/entry}"'
/opt_speculate_debug && self->tracing/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( speculate( self->speculation ) )\n", self->indent, "{",
		'"${ru_pv_t}"' ) : 0;
}
'

		prx_inside='
'

		prx_return='
'"${prx_probes//<ername>/return}"'
/opt_speculate && self->tracing == 1/
{
	'"${speculate_true:+commit( self->speculation );}"'
	self->speculation = 0;
}

'"${prx_probes//<ername>/return}"'
/opt_speculate_debug && self->tracing == 1/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( commit( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

'"${prx_probes//<ername>/return}"'
/self->tracing/
{
	self->tracing = self->tracing - 1;
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
}
'
	}

	local trace_entry trace_return trace_inside
	"${trace}" && {
		trace_entry='
fbt:zfs:arc_read:entry
/self->tracing/
{
	/*
		args[0]: zio_t *
		args[1]: spa_t *
		args[2]: blkptr_t *
		args[3]: uint64_t
		args[4]: arc_done_func_t *
		args[5]: void *
		args[6]: int
		args[7]: enum zio_flag
		args[8]: arc_options_t
		args[9]: zbookmark_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'( arc_options = %i, zio_flag = %i )\n", self->indent, "{",
		'"${ru_pv_t}"', self->arc_options, args[7] );
	self->captured_entry = 1;
}

fbt:zfs:arc_get_data_block:entry
/self->tracing/
{
	/*
		args[0]: uint64_t
		args[1]: boolean_t
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d, args[1] = %d )\n", self->indent, "{",
		'"${ru_pv_t}"', args[0], args[1] );
	self->captured_entry = 1;
}

fbt:zfs:zio_data_buf_alloc:entry
/self->tracing/
{
	/*
		args[0]: size_t
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d )\n", self->indent, "{",
		'"${ru_pv_t}"', args[0] );
	self->captured_entry = 1;
}

fbt:zfs:zio_read:entry
/self->tracing/
{
	/*
		args[0]: zio_t *
		args[1]: spa_t *
		args[2]: blkptr_t *
		args[3]: void *
		args[4]: uint64_t
		args[5]: zio_done_func_t *
		args[6]: void *
		args[7]: int
		args[8]: enum zio_flag
		args[9]: zbookmark_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'( (enum zio_flag)args[8] = %d )\n", self->indent, "{",
		'"${ru_pv_t}"', args[8] );
	self->captured_entry = 1;
}

fbt:zfs:dmu_buf_rele_array:entry
/self->tracing/
{
	/*
		args[0]: dmu_buf_t **
		args[1]: int
		args[2]: void *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'( (dmu_buf_t**)args[0] = %p, args[1] = %d, args[2] = %p )\n", self->indent, "{",
		'"${ru_pv_t}"',
		args[0], args[1], args[2] );
	self->captured_entry = 1;
}

/* probes that dont always have entry/return */
fbt:unix:i_ddi_splx:entry
/self->tracing/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "|",
		'"${ru_pv_t}"' );
	self->captured_entry = 1;
}

syscall:::entry,
fbt:::entry
/self->tracing && !self->captured_entry/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	self->indent = self->indent + 1;
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "{",
		'"${ru_pv_t}"' );
}

syscall:::entry,
fbt:::entry
/self->tracing && self->captured_entry/
{
	self->captured_entry = 0;
}

fbt:zfs:dbuf_evict:entry,
fbt:zfs:dbuf_evict_user:entry,
fbt:zfs:dbuf_is_metadata:entry,
fbt:zfs:dbuf_rele:entry,
fbt:zfs:dbuf_rele_and_unlock:entry,
fbt:zfs:dbuf_read:entry
/self->tracing/
{
	/*
		args[0]: dmu_buf_impl_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	this->db_objset = ( struct objset* )( ( dmu_buf_impl_t* )args[0] && args[0]->db_objset ? args[0]->db_objset : NULL );
	this->dsl_dataset = ( struct dsl_dataset* )( this->db_objset && this->db_objset->os_dsl_dataset ? this->db_objset->os_dsl_dataset : NULL );

	this->ds_dir_0 = ( struct dsl_dir* )( this->dsl_dataset && this->dsl_dataset->ds_dir ? this->dsl_dataset->ds_dir : NULL );

	this->ds_dir_1 = ( struct dsl_dir* )( this->ds_dir_0 && this->ds_dir_0->dd_parent ? this->ds_dir_0->dd_parent : NULL );
	this->ds_dir_2 = ( struct dsl_dir* )( this->ds_dir_1 && this->ds_dir_1->dd_parent ? this->ds_dir_1->dd_parent : NULL );
	this->ds_dir_3 = ( struct dsl_dir* )( this->ds_dir_2 && this->ds_dir_2->dd_parent ? this->ds_dir_2->dd_parent : NULL );
	this->ds_dir_4 = ( struct dsl_dir* )( this->ds_dir_3 && this->ds_dir_3->dd_parent ? this->ds_dir_3->dd_parent : NULL );
	this->ds_dir_5 = ( struct dsl_dir* )( this->ds_dir_4 && this->ds_dir_4->dd_parent ? this->ds_dir_4->dd_parent : NULL );
	this->ds_dir_6 = ( struct dsl_dir* )( this->ds_dir_5 && this->ds_dir_5->dd_parent ? this->ds_dir_5->dd_parent : NULL );

	printf( "%*s'"${ru_pf_t}"'( db = %p, db.level = %d, db.immediate_evict = %d, db.dirtycnt = %d, os.name = %s/%s/%s/%s/%s/%s/%s, os.pc = %d, dn_type = %d, dn_type.m = %d, dn_type.n = %s, arc_ref = %p, arc_buf = %p )\n", self->indent, ":",
		'"${ru_pv_t}"',
		args[0],
		( args[0] ? args[0]->db_level : -1 ),
		( args[0] ? args[0]->db_immediate_evict : -1 ),
		( args[0] ? args[0]->db_dirtycnt : -1 ),
		( this->ds_dir_0 ? this->ds_dir_0->dd_myname : "<null>" ),
		( this->ds_dir_1 ? this->ds_dir_1->dd_myname : "<null>" ),
		( this->ds_dir_2 ? this->ds_dir_2->dd_myname : "<null>" ),
		( this->ds_dir_3 ? this->ds_dir_3->dd_myname : "<null>" ),
		( this->ds_dir_4 ? this->ds_dir_4->dd_myname : "<null>" ),
		( this->ds_dir_5 ? this->ds_dir_5->dd_myname : "<null>" ),
		( this->ds_dir_6 ? this->ds_dir_6->dd_myname : "<null>" ),
		( this->db_objset ? args[0]->db_objset->os_primary_cache : -1 ),
		( args[0] && args[0]->db_dnode_handle && args[0]->db_dnode_handle->dnh_dnode ? args[0]->db_dnode_handle->dnh_dnode->dn_type : -1 ),
		( args[0] && args[0]->db_dnode_handle && args[0]->db_dnode_handle->dnh_dnode ? `dmu_ot[ args[0]->db_dnode_handle->dnh_dnode->dn_type ].ot_metadata : -1 ),
		( args[0] && args[0]->db_dnode_handle && args[0]->db_dnode_handle->dnh_dnode ? `dmu_ot[ args[0]->db_dnode_handle->dnh_dnode->dn_type ].ot_name : "null" ),
		( args[0] ? args[0]->db_ref : 0 ), ( args[0] && args[0]->db_ref ? args[0]->db_ref->r_buf : 0 ) );
}

fbt:zfs:arc_reactivate_ref:entry
/self->tracing/
{
	/*
		args[0]: arc_ref_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( arc_flags = %i )\n", self->indent, ":",
		'"${ru_pv_t}"', self->reactivate_arc_flags );
}

fbt:zfs:arc_data_free:entry,
fbt:zfs:arc_add_to_evictables:entry,
fbt:zfs:arc_kill_buf:entry,
fbt:zfs:arc_hold:entry,
fbt:zfs:arc_rele:entry
/self->tracing/
{
	/*
		args[0]: arc_buf_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( arc_buf = %p, s_state = %d, ev.s = %d, ev.i.s = %d, ev.r.s = %d )\n", self->indent, ":",
		'"${ru_pv_t}"',
		args[0], args[0]->b_state->s_state, args[0]->b_state->s_evictable.l_size, args[0]->b_state->s_evictable.l_insert.list_size, args[0]->b_state->s_evictable.l_remove.list_size );
}

fbt:zfs:arc_destroy_ref:entry,
fbt:zfs:arc_evict_ref:entry,
fbt:zfs:arc_remove_ref:entry,
fbt:zfs:arc_reactivate_ref:entry,
fbt:zfs:arc_inactivate_ref:entry
/self->tracing/
{
	/*
		args[0]: arc_ref_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( arc_ref = %p, arc_buf = %p )\n", self->indent, ":",
		'"${ru_pv_t}"',
		args[0], args[0]->r_buf );
}
'

		trace_return='
fbt:zfs:arc_read:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: int
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( arc_flags = %i, read_hit = %i )\n", self->indent, "}",
		'"${ru_pv_t}"', self->read_arc_flags, self->read_hit );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

fbt:zfs:arc_reactivate_ref:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: int
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( arc_flags = %i, hit = %i )\n", self->indent, "}",
		'"${ru_pv_t}"', self->reactivate_arc_flags, self->reactivate_hit );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

fbt:zfs:arc_get_data_block:return,
fbt:zfs:zio_data_buf_alloc:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: void *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d, (void*)args[1] = %p )\n", self->indent, "}",
		'"${ru_pv_t}"', args[0], args[1] );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

fbt:zfs:dbuf_is_metadata:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: boolean_t
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d, (boolean_t)args[1] = %d )\n", self->indent, "}",
		'"${ru_pv_t}"', args[0], args[1] );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

fbt:zfs:arc_ref_anonymous:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: int
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d, (int)args[1] = %d )\n", self->indent, "}",
		'"${ru_pv_t}"', args[0], args[1] );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

fbt:zfs:zio_read:return
/self->tracing/
{
	/*
		args[0]: int
		args[1]: zio_t *
	*/
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'( args[0] = %d, (zio_t*)args[1] = %p )\n", self->indent, "}",
		'"${ru_pv_t}"', args[0], args[1] );
	self->indent = self->indent - 1;
	self->captured_return = 1;
}

/* probes that dont always have entry/return */
fbt:unix:i_ddi_splx:return
/self->tracing/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "|",
		'"${ru_pv_t}"' );
	self->captured_return = 1;
}

syscall:::return,
fbt:::return
/self->tracing && !self->captured_return/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "}",
		'"${ru_pv_t}"' );
	self->indent = self->indent - 1;
}

syscall:::return,
fbt:::return
/self->tracing && self->captured_return/
{
	self->captured_return = 0;
}
'

		trace_inside='
sdt:::
/self->tracing/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "|",
		'"${ru_pv_t}"' );
}

fsinfo:genunix::
/self->tracing/
{
	'"${speculate_true:+speculate( self->speculation );}"'
	printf( "%*s'"${ru_pf_t}"'()\n", self->indent, "|",
		'"${ru_pv_t}"' );
}
'
	}

	local duration_script

	"${duration}" && {
		duration_script='
profile:::tick-1sec
{
	duration--;
}

profile:::tick-1sec
/duration < 0/
{
	exit( 0 );
}
'
	}

#pragma D option flowindent

	program='
#pragma D option bufsize=16M
'"${speculate_true:+#pragma D option specsize=16M}"'
'"${speculate_true:+#pragma D option nspec=${nspec}}"'
'"${speculate_true:+#pragma D option bufresize=manual}"'
'"${speculate_true:+#pragma D option cleanrate=200hz}"'
inline int opt_debug = '"${debug_true:-0}"';
inline int opt_trace = '"${trace_true:-0}"';
inline int opt_speculate = '"${speculate_true:-0}"';
inline int opt_speculate_debug = '"${speculate_debug_true:-0}"';
self int has_dmu_buf_impl;
self dmu_buf_impl_t* dmu_buf_impl;
self int tracing;
self int indent;
self int read_hit;
self int reactivate_hit;
self int arc_options;
self int reactivate_arc_flags;
self int read_arc_flags;
self int speculation;
self int captured_entry;
self int captured_return;
int duration;

dtrace:::BEGIN
{
	'"${duration_true:+duration = ${duration_seconds};}"'
	printf( "'"${ru_pf_t}"'()\n",
		'"${ru_pv_t}"' );
}

syscall::exec*:return
/opt_trace && '"${predicate}"'/
{
	this->argci = -1;
	printf( "%s'"${ru_pf_t}"'( argc = %d )\n", "|",
		'"${ru_pv_t}"', curpsinfo->pr_argc );
	printf( "%s'"${ru_pf_t}"'( argv = { %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s, %d: %s } )\n", "|",
		'"${ru_pv_t}"'
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" )
	);
	/*
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? "x" : "NULL" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? "x" : "NULL" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? "x" : "NULL" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? "x" : "NULL" )
		, ++this->argci, ( curpsinfo->pr_argc > this->argci ? "x" : "NULL" )
	*/
	/*
		, this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 8, 8) ) : "" )
		, this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 8, 8) ) : "" )
		, this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 8, 8) ) : "" )
		, this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 8, 8) ) : "" )
		, this->argci, ( curpsinfo->pr_argc > this->argci ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + ( this->argci - 1 ) * 8, 8) ) : "" )
	*/
	/*
		,( curpsinfo->pr_argc > this->argci++ ? this->argci : -1 ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? this->argci : -1 ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? this->argci : -1 ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? this->argci : -1 ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? this->argci : -1 ), this->argci
	*/
	/*
	printf( "%s'"${ru_pf_t}"'( argv = >%s<%d >%s<%d >%s<%d )\n", "|",
		'"${ru_pv_t}"'
		,( curpsinfo->pr_argc > this->argci++ ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" ), this->argci
		,( curpsinfo->pr_argc > this->argci++ ? copyinstr( (uintptr_t) curpsinfo->pr_dmodel == PR_MODEL_ILP32 ? *(uint32_t*)copyin(curpsinfo->pr_argv + this->argci * 4, 4) : *(uint64_t*)copyin(curpsinfo->pr_argv + this->argci * 8, 8) ) : "" ), this->argci
	);
	*/
}

'"${duration_script}"'

/*
'${debug_true:+ '
fbt:zfs:arc_read:entry,
fbt:zfs:arc_reactivate_ref:entry
{
	printf( "'"${ru_pf_t}"'( execname = %s )\n",
		'"${ru_pv_t}"', execname );
}'
}'
*/

fbt:zfs:arc_read:entry,
fbt:zfs:arc_reactivate_ref:entry
/opt_speculate && '"${duration_true:+duration >= 0 && }${predicate}"' && !self->tracing && !self->speculation/
{
	self->speculation = speculation();
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->speculation = ... )\n",
		'"${ru_pv_t}"' );'}'
}

fbt:zfs:arc_read:entry,
fbt:zfs:arc_reactivate_ref:entry
/'"${speculate_false:+${duration_true:+duration >= 0 && }${predicate}}${speculate_true:+self->speculation}"'/
{
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	self->tracing = self->tracing + 1;
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
}

fbt:zfs:arc_read:entry,
fbt:zfs:arc_reactivate_ref:entry
/opt_speculate_debug && self->tracing/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( speculate( self->speculation ) )\n", self->indent, "{",
		'"${ru_pv_t}"' ) : 0;
}

'"${prx_entry}"'
'"${pri_entry}"'

fbt:zfs:arc_read:entry
/'"${speculate_false:+${duration_true:+duration >= 0 && }${predicate}}${speculate_true:+self->speculation}"'/
{
	/*
		args[0]: zio_t *
		args[1]: spa_t *
		args[2]: blkptr_t *
		args[3]: uint64_t
		args[4]: arc_done_func_t *
		args[5]: void *
		args[6]: int
		args[7]: enum zio_flag
		args[8]: arc_options_t
		args[9]: zbookmark_t *
	*/
	self->read_hit = -1;
	self->read_arc_flags = -1;
	self->arc_options = args[8];
}

fbt:zfs:arc_reactivate_ref:entry
/'"${speculate_false:+${duration_true:+duration >= 0 && }${predicate}}${speculate_true:+self->speculation}"'/
{
	/*
		args[0]: arc_ref_t *
	*/
	self->reactivate_hit = -1;
	self->reactivate_arc_flags = args[0]->r_buf->b_id.h_flags;
}

'"${trace_entry}"'

/* inside below */

sdt:zfs:arc_read:arc-miss
/self->tracing/
{
	self->read_hit = 1;
}

sdt:zfs:arc_read:arc-hit
/self->tracing/
{
	self->read_hit = 2;
}

sdt:zfs:arc_reactivate_ref:arc-hit
/self->tracing/
{
	self->reactivate_hit = 2;
}

sdt:zfs:arc_read:arc-miss,
sdt:zfs:arc_read:arc-hit,
sdt:zfs:arc_reactivate_ref:arc-hit
/self->tracing == 0'"${duration_true:+ && duration >= 0}${predicate:+ && ${predicate}}"'/
{
	opt_trace ? printf( "'"${ru_pf_t}"'()\n",
		'"${ru_pv_t}"' ) : 0;
}

'"${trace_inside}"'

/* return below */

fbt:zfs:arc_hash_lookup:return
/self->tracing && self->read_arc_flags == -1/
{
	/*
		args[0]: int
		args[1]: arc_buf_t *
	*/
	self->read_arc_flags = args[1]->b_id.h_flags;
}

fbt:zfs:arc_hole_ref:return
/self->tracing && self->read_arc_flags == -1/
{
	/*
		args[0]: int
		args[1]: arc_ref_t *
	*/
	self->read_arc_flags = args[1]->r_buf->b_id.h_flags;
}

fbt:zfs:arc_read:return
/opt_speculate && self->tracing == 1 && '"( ${read_filter_predicate} && ${read_loc_predicate} )"'/
{
	'"${speculate_true:+commit( self->speculation );}"'
	self->speculation = 0;
}

fbt:zfs:arc_read:return
/opt_speculate_debug && self->tracing == 1 && '"( ${read_filter_predicate} && ${read_loc_predicate} )"'/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( commit( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

fbt:zfs:arc_read:return
/opt_speculate && self->tracing == 1 && '"!( ${read_filter_predicate} && ${read_loc_predicate} )"'/
{
	'"${speculate_true:+discard( self->speculation );}"'
	self->speculation = 0;
}

fbt:zfs:arc_read:return
/opt_speculate_debug && self->tracing == 1 && '"!( ${read_filter_predicate} && ${read_loc_predicate} )"'/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( discard( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

fbt:zfs:arc_reactivate_ref:return
/opt_speculate && self->tracing == 1 && '"( ${reactivate_filter_predicate} && ${reactivate_loc_predicate} )"'/
{
	'"${speculate_true:+commit( self->speculation );}"'
	self->speculation = 0;
}

fbt:zfs:arc_reactivate_ref:return
/opt_speculate_debug && self->tracing == 1 && '"( ${reactivate_filter_predicate} && ${reactivate_loc_predicate} )"'/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( commit( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

fbt:zfs:arc_reactivate_ref:return
/opt_speculate && self->tracing == 1 && '"!( ${reactivate_filter_predicate} && ${reactivate_loc_predicate} )"'/
{
	'"${speculate_true:+discard( self->speculation );}"'
	self->speculation = 0;
}

fbt:zfs:arc_reactivate_ref:return
/opt_speculate_debug && self->tracing == 1 && '"!( ${reactivate_filter_predicate} && ${reactivate_loc_predicate} )"'/
{
	opt_trace ? printf( "%*s'"${ru_pf_t}"'( discard( self->speculation ) )\n", self->indent, "}",
		'"${ru_pv_t}"' ) : 0;
}

fbt:zfs:arc_read:return
/self->tracing && '"${read_filter_predicate} && ${read_loc_predicate}"'/
{
	@out[ execname, pid, probefunc, self->arc_options, self->read_arc_flags, self->read_hit'"${kernel_stack_part}"''"${user_stack_part}"' ] = count();
}

fbt:zfs:arc_reactivate_ref:return
/self->tracing && '"${reactivate_filter_predicate} && ${reactivate_loc_predicate}"'/
{
	@out[ execname, pid, probefunc, -1, self->reactivate_arc_flags, self->reactivate_hit'"${kernel_stack_part}"''"${user_stack_part}"' ] = count();
}

'"${trace_return}"'
'"${prx_return}"'
'"${pri_return}"'

fbt:zfs:arc_reactivate_ref:return,
fbt:zfs:arc_read:return
/self->tracing/
{
	self->tracing = self->tracing - 1;
	self->indent = ( self->tracing == 0 ? 0 : self->indent );
	'${debug_true:+ 'printf( "'"${ru_pf_t}"'( self->tracing = ... )\n",
		'"${ru_pv_t}"' );'}'
}

dtrace:::END
{
	printf( "execname, pid, probefunc, arc_options (1=meta), arc_flags (1=meta), arc_hit(1=miss, 2=hit, *=unk), kstack, ustack, count" );
	printa( @out );
	/*
	printf( "stacks:" );
	printa( @stacks );
	*/
}
'

	#verbose_dump_vars 1 program
	echo "${program}" | gawk '{ printf( "%04d:\t\t%s\n", NR, $0 ) }' 1>&2
	echo "${program}" | /usr/sbin/dtrace -64 -q -s /dev/stdin ${output_file_set_true:+-o ${output_file}}
}

arcsnoop_main "${@}"

exit 0;

__comment='
/*
# echo "1000 $w; ::print -at arc_ref_t" | mdb -k
{
    0 uint64_t r_size 
    8 void *r_data 
    10 arc_evict_func_t *r_efunc 
    18 void *r_private 
    20 arc_buf_t *r_buf 
    28 list_node_t r_link {
        28 struct list_node *list_next 
        30 struct list_node *list_prev 
    }
    38 krwlock_t r_lock {
        38 void *[1] _opaque 
    }
}

# echo "1000 $w; ::print -at arc_buf_t" | mdb -k
{
    0 arc_hdr_t b_id {
        0 uint64_t h_spa 
        8 dva_t h_dva {
            8 uint64_t [2] dva_word 
        }
        18 uint64_t h_birth 
        20 arc_flags_t h_flags 
        24 uint32_t h_hash_idx 
        28 arc_hdr_t *h_next 
    }
    30 uint64_t b_size 
    38 arc_state_t *b_state 
    40 list_t b_inactive {
        40 size_t list_size 
        48 size_t list_offset 
        50 struct list_node list_head {
            50 struct list_node *list_next 
            58 struct list_node *list_prev 
        }
    }
    60 void *b_data 
    68 clock_t b_last_access 
    70 arc_cksum_t *b_cksum 
    78 arc_elink_t *b_link 
    80 void *b_cookie 
    88 refcount_t b_bcount {
        88 uint64_t rc_count 
    }
    90 kmutex_t b_lock {
        90 void *[1] _opaque 
    }
}

#  echo "1000 $w; ::print -at arc_state_t" | mdb -k
{
    0 uint64_t s_size 
    8 arc_evict_list_t s_evictable {
        8 uint64_t l_generation 
        10 uint64_t l_size 
        18 kmutex_t l_link_lock {
            18 void *[1] _opaque 
        }
        20 kmutex_t l_update_lock {
            20 void *[1] _opaque 
        }
        28 list_t l_insert {
            28 size_t list_size 
            30 size_t list_offset 
            38 struct list_node list_head {
                38 struct list_node *list_next 
                40 struct list_node *list_prev 
            }
        }
        48 kmutex_t l_evict_lock {
            48 void *[1] _opaque 
        }
        50 list_t l_remove {
            50 size_t list_size 
            58 size_t list_offset 
            60 struct list_node list_head {
                60 struct list_node *list_next 
                68 struct list_node *list_prev 
            }
        }
    }
    70 arc_state_enum_t s_state 
}

# echo "1000 $w; ::print -at dmu_buf_impl_t" | mdb -k
{
    0 dmu_buf_t db {
        0 uint64_t db_object 
        8 uint64_t db_offset 
        10 uint64_t db_size 
        18 void *db_data 
    }
    20 struct objset *db_objset 
    28 struct dnode_handle *db_dnode_handle 
    30 dmu_buf_impl_t *db_parent 
    38 dmu_buf_impl_t *db_hash_next 
    40 uint64_t db_blkid 
    48 uint32_t db_blkoff 
    4c uint8_t db_level 
    50 kmutex_t db_mtx {
        50 void *[1] _opaque 
    }
    58 uint64_t db_birth 
    60 dbuf_states_t db_state 
    68 refcount_t db_holds {
        68 uint64_t rc_count 
    }
    70 uint64_t db_writers_waiting 
    78 arc_ref_t *db_ref 
    80 kcondvar_t db_changed {
        80 ushort_t _opaque 
    }
    88 dbuf_dirty_record_t *db_last_dirty 
    90 dbuf_dirty_record_t *db_data_pending 
    98 list_node_t db_link {
        98 struct list_node *list_next 
        a0 struct list_node *list_prev 
    }
    a8 void *db_user_ptr 
    b0 void **db_user_data_ptr_ptr 
    b8 dmu_buf_evict_func_t *db_evict_func 
    c0 uint8_t db_immediate_evict 
    c1 uint8_t db_freed_in_flight 
    c2 uint8_t db_managed 
    c3 uint8_t db_dirtycnt 
}

# echo "1000 $w; ::print -at \"struct dnode_handle\"" | mdb -k
{
    0 krwlock_t dnh_rwlock {
        0 void *[1] _opaque 
    }
    8 dnode_t *dnh_dnode 
}

# echo "1000 $w; ::print -at \"struct dnode\"" | mdb -k
{
    0 krwlock_t dn_struct_rwlock {
        0 void *[1] _opaque 
    }
    8 list_node_t dn_link {
        8 struct list_node *list_next 
        10 struct list_node *list_prev 
    }
    18 struct objset *dn_objset 
    20 uint64_t dn_object 
    28 struct dmu_buf_impl *dn_dbuf 
    30 struct dnode_handle *dn_handle 
    38 krwlock_t *dn_phys_rwlock 
    40 dnode_phys_t *dn_phys 
    48 dmu_object_type_t dn_type 
    4c uint16_t dn_bonuslen 
    4e uint8_t dn_bonustype 
    4f uint8_t dn_nblkptr 
    50 uint8_t dn_checksum 
    51 uint8_t dn_compress 
    52 uint8_t dn_nlevels 
    53 uint8_t dn_indblkshift 
    54 uint8_t dn_datablkshift 
    55 uint8_t dn_moved 
    56 uint16_t dn_datablkszsec 
    58 uint32_t dn_datablksz 
    60 uint64_t dn_maxblkid 
    68 uint8_t [4] dn_next_nblkptr 
    6c uint8_t [4] dn_next_nlevels 
    70 uint8_t [4] dn_next_indblkshift 
    74 uint8_t [4] dn_next_bonustype 
    78 uint8_t [4] dn_rm_spillblk 
    7c uint16_t [4] dn_next_bonuslen 
    84 uint32_t [4] dn_next_blksz 
    94 uint32_t dn_dbufs_count 
    98 list_node_t [4] dn_dirty_link 
    d8 kmutex_t dn_mtx {
        d8 void *[1] _opaque 
    }
    e0 list_t [4] dn_dirty_records 
    160 avl_tree_t [4] dn_ranges 
    200 uint64_t dn_allocated_txg 
    208 uint64_t dn_free_txg 
    210 uint64_t dn_assigned_txg 
    218 kcondvar_t dn_notxholds {
        218 ushort_t _opaque 
    }
    21c enum dnode_dirtycontext dn_dirtyctx 
    220 uint8_t *dn_dirtyctx_firstset 
    228 refcount_t dn_tx_holds {
        228 uint64_t rc_count 
    }
    230 refcount_t dn_holds {
        230 uint64_t rc_count 
    }
    238 kmutex_t dn_dbufs_mtx {
        238 void *[1] _opaque 
    }
    240 list_t dn_dbufs {
        240 size_t list_size 
        248 size_t list_offset 
        250 struct list_node list_head {
            250 struct list_node *list_next 
            258 struct list_node *list_prev 
        }
    }
    260 struct dmu_buf_impl *dn_bonus 
    268 boolean_t dn_have_spill 
    270 zio_t *dn_zio 
    278 uint64_t dn_oldused 
    280 uint64_t dn_oldflags 
    288 uint64_t dn_olduid 
    290 uint64_t dn_oldgid 
    298 uint64_t dn_newuid 
    2a0 uint64_t dn_newgid 
    2a8 int dn_id_flags 
    2b0 struct zfetch dn_zfetch {
        2b0 krwlock_t zf_rwlock {
            2b0 void *[1] _opaque 
        }
        2b8 list_t zf_stream {
            2b8 size_t list_size 
            2c0 size_t list_offset 
            2c8 struct list_node list_head {
                2c8 struct list_node *list_next 
                2d0 struct list_node *list_prev 
            }
        }
        2d8 struct dnode *zf_dnode 
        2e0 uint32_t zf_stream_cnt 
        2e8 uint64_t zf_alloc_fail 
    }
}

# echo "1000 $w; ::print -at zio_t" | mdb -k
{
    0 zbookmark_t io_bookmark {
        0 uint64_t zb_objset 
        8 uint64_t zb_object 
        10 int64_t zb_level 
        18 uint64_t zb_blkid 
    }
    20 zio_prop_t io_prop {
        20 enum zio_checksum zp_checksum 
        24 enum zio_compress zp_compress 
        28 enum zio_crypt zp_crypt 
        2c dmu_object_type_t zp_type 
        30 uint8_t zp_level 
        31 uint8_t zp_copies 
        32 uint8_t zp_dedup 
        33 uint8_t zp_dedup_verify 
    }
    34 zio_type_t io_type 
    38 enum zio_child io_child_type 
    3c int io_cmd 
    40 uint8_t io_priority 
    41 uint8_t io_reexecute 
    42 uint8_t [2] io_state 
    48 uint64_t io_txg 
    50 spa_t *io_spa 
    58 blkptr_t *io_bp 
    60 blkptr_t *io_bp_override 
    68 blkptr_t io_bp_copy {
        68 dva_t [3] blk_dva 
        98 uint64_t blk_prop 
        a0 uint64_t [2] blk_pad 
        b0 uint64_t blk_phys_birth 
        b8 uint64_t blk_birth 
        c0 uint64_t blk_fill 
        c8 zio_cksum_t blk_cksum {
            c8 uint64_t [4] zc_word 
        }
    }
    e8 list_t io_parent_list {
        e8 size_t list_size 
        f0 size_t list_offset 
        f8 struct list_node list_head {
            f8 struct list_node *list_next 
            100 struct list_node *list_prev 
        }
    }
    108 list_t io_child_list {
        108 size_t list_size 
        110 size_t list_offset 
        118 struct list_node list_head {
            118 struct list_node *list_next 
            120 struct list_node *list_prev 
        }
    }
    128 zio_link_t *io_walk_link 
    130 zio_t *io_logical 
    138 zio_transform_t *io_transform_stack 
    140 zio_done_func_t *io_ready 
    148 zio_done_func_t *io_done 
    150 void *io_private 
    158 int64_t io_prev_space_delta 
    160 blkptr_t io_bp_orig {
        160 dva_t [3] blk_dva 
        190 uint64_t blk_prop 
        198 uint64_t [2] blk_pad 
        1a8 uint64_t blk_phys_birth 
        1b0 uint64_t blk_birth 
        1b8 uint64_t blk_fill 
        1c0 zio_cksum_t blk_cksum {
            1c0 uint64_t [4] zc_word 
        }
    }
    1e0 void *io_data 
    1e8 void *io_orig_data 
    1f0 uint64_t io_size 
    1f8 uint64_t io_orig_size 
    200 list_t io_join_list {
        200 size_t list_size 
        208 size_t list_offset 
        210 struct list_node list_head {
            210 struct list_node *list_next 
            218 struct list_node *list_prev 
        }
    }
    220 list_node_t io_join_node {
        220 struct list_node *list_next 
        228 struct list_node *list_prev 
    }
    230 vdev_t *io_vd 
    238 vdev_ops_t *io_vd_ops 
    240 void *io_vsd 
    248 const zio_vsd_ops_t *io_vsd_ops 
    250 uint64_t io_offset 
    258 uint64_t io_deadline 
    260 avl_node_t io_offset_node {
        260 struct avl_node *[2] avl_child 
        270 uintptr_t avl_pcb 
    }
    278 avl_node_t io_deadline_node {
        278 struct avl_node *[2] avl_child 
        288 uintptr_t avl_pcb 
    }
    290 avl_tree_t *io_vdev_tree 
    298 enum zio_flag io_flags 
    29c enum zio_stage io_stage 
    2a0 enum zio_stage io_pipeline 
    2a4 enum zio_flag io_orig_flags 
    2a8 enum zio_stage io_orig_stage 
    2ac enum zio_stage io_orig_pipeline 
    2b0 int io_error 
    2b4 int [4] io_child_error 
    2c8 uint64_t [4] io_children 
    308 uint64_t io_child_count 
    310 uint64_t io_parent_count 
    318 uint64_t *io_stall 
    320 zio_t *io_gang_leader 
    328 zio_gang_node_t *io_gang_tree 
    330 void *io_executor 
    338 void *io_waiter 
    340 kmutex_t io_lock {
        340 void *[1] _opaque 
    }
    348 kcondvar_t io_cv {
        348 ushort_t _opaque 
    }
    350 zio_cksum_report_t *io_cksum_report 
    358 uint64_t io_ena 
}

# echo "1000 $w; ::print -at \"struct objset\"" | mdb -k
{
    0 struct dsl_dataset *os_dsl_dataset 
    8 spa_t *os_spa 
    10 arc_ref_t *os_phys_buf 
    18 objset_phys_t *os_phys 
    20 dnode_handle_t os_meta_dnode {
        20 krwlock_t dnh_rwlock {
            20 void *[1] _opaque 
        }
        28 dnode_t *dnh_dnode 
    }
    30 dnode_handle_t os_userused_dnode {
        30 krwlock_t dnh_rwlock {
            30 void *[1] _opaque 
        }
        38 dnode_t *dnh_dnode 
    }
    40 dnode_handle_t os_groupused_dnode {
        40 krwlock_t dnh_rwlock {
            40 void *[1] _opaque 
        }
        48 dnode_t *dnh_dnode 
    }
    50 zilog_t *os_zil 
    58 uint8_t os_checksum 
    59 uint8_t os_compress 
    5a uint8_t os_copies 
    5b uint8_t os_dedup_checksum 
    5c uint8_t os_dedup_verify 
    5d uint8_t os_logbias 
    5e uint8_t os_primary_cache 
    5f uint8_t os_secondary_cache 
    60 uint8_t os_sync 
    61 uint8_t os_crypt 
    68 struct dmu_tx *os_synctx 
    70 uint64_t os_last_sync 
    78 boolean_t os_initializing 
    80 zil_header_t os_zil_header {
        80 uint64_t zh_claim_txg 
        88 uint64_t zh_replay_seq 
        90 blkptr_t zh_log {
            90 dva_t [3] blk_dva 
            c0 uint64_t blk_prop 
            c8 uint64_t [2] blk_pad 
            d8 uint64_t blk_phys_birth 
            e0 uint64_t blk_birth 
            e8 uint64_t blk_fill 
            f0 zio_cksum_t blk_cksum {
                f0 uint64_t [4] zc_word 
            }
        }
        110 uint64_t zh_claim_blk_seq 
        118 uint64_t zh_flags 
        120 uint64_t zh_claim_lr_seq 
        128 uint64_t [3] zh_pad 
    }
    140 list_t os_synced_dnodes {
        140 size_t list_size 
        148 size_t list_offset 
        150 struct list_node list_head {
            150 struct list_node *list_next 
            158 struct list_node *list_prev 
        }
    }
    160 uint64_t os_flags 
    168 kmutex_t os_obj_lock {
        168 void *[1] _opaque 
    }
    170 uint64_t os_obj_next 
    178 kmutex_t os_lock {
        178 void *[1] _opaque 
    }
    180 list_t [4] os_dirty_dnodes 
    200 list_t [4] os_free_dnodes 
    280 list_t os_dnodes {
        280 size_t list_size 
        288 size_t list_offset 
        290 struct list_node list_head {
            290 struct list_node *list_next 
            298 struct list_node *list_prev 
        }
    }
    2a0 list_t os_downgraded_dbufs {
        2a0 size_t list_size 
        2a8 size_t list_offset 
        2b0 struct list_node list_head {
            2b0 struct list_node *list_next 
            2b8 struct list_node *list_prev 
        }
    }
    2c0 kmutex_t os_user_ptr_lock {
        2c0 void *[1] _opaque 
    }
    2c8 void *os_user_ptr 
    2d0 sa_os_t *os_sa 
    2d8 boolean_t os_destroy_nokey 
}

# echo "1000 $w; ::print -at \"struct dsl_dataset\"" | mdb -k
{
    0 struct dsl_dir *ds_dir 
    8 dsl_dataset_phys_t *ds_phys 
    10 dmu_buf_t *ds_dbuf 
    18 uint64_t ds_object 
    20 uint64_t ds_fsid_guid 
    28 struct dsl_dataset *ds_prev 
    30 dsl_deadlist_t ds_deadlist {
        30 objset_t *dl_os 
        38 uint64_t dl_object 
        40 avl_tree_t dl_tree {
            40 struct avl_node *avl_root 
            48 int (*)() avl_compar 
            50 size_t avl_offset 
            58 ulong_t avl_numnodes 
            60 size_t avl_size 
        }
        68 boolean_t dl_havetree 
        70 struct dmu_buf *dl_dbuf 
        78 dsl_deadlist_phys_t *dl_phys 
        80 kmutex_t dl_lock {
            80 void *[1] _opaque 
        }
        88 bpobj_t dl_bpobj {
            88 kmutex_t bpo_lock {
                88 void *[1] _opaque 
            }
            90 objset_t *bpo_os 
            98 uint64_t bpo_object 
            a0 int bpo_epb 
            a4 uint8_t bpo_havecomp 
            a5 uint8_t bpo_havesubobj 
            a8 bpobj_phys_t *bpo_phys 
            b0 dmu_buf_t *bpo_dbuf 
            b8 dmu_buf_t *bpo_cached_dbuf 
        }
        c0 boolean_t dl_oldfmt 
    }
    c8 bplist_t ds_pending_deadlist {
        c8 kmutex_t bpl_lock {
            c8 void *[1] _opaque 
        }
        d0 list_t bpl_list {
            d0 size_t list_size 
            d8 size_t list_offset 
            e0 struct list_node list_head {
                e0 struct list_node *list_next 
                e8 struct list_node *list_prev 
            }
        }
    }
    f0 kmutex_t ds_recvlock {
        f0 void *[1] _opaque 
    }
    f8 txg_node_t ds_dirty_link {
        f8 struct txg_node *[4] tn_next 
        118 uint8_t [4] tn_member 
    }
    120 list_node_t ds_synced_link {
        120 struct list_node *list_next 
        128 struct list_node *list_prev 
    }
    130 kmutex_t ds_lock {
        130 void *[1] _opaque 
    }
    138 objset_t *ds_objset 
    140 uint64_t ds_userrefs 
    148 int64_t ds_send_cnt 
    150 krwlock_t ds_rwlock {
        150 void *[1] _opaque 
    }
    158 kcondvar_t ds_exclusive_cv {
        158 ushort_t _opaque 
    }
    160 void *ds_owner 
    168 uint64_t ds_trysnap_txg 
    170 kmutex_t ds_opening_lock {
        170 void *[1] _opaque 
    }
    178 uint64_t ds_reserved 
    180 uint64_t ds_quota 
    188 boolean_t ds_destroying 
    18c char [256] ds_snapname 
}

# echo "1000 $w; ::print -at \"struct dsl_dir\"" | mdb -k
{
    0 uint64_t dd_object 
    8 dsl_dir_phys_t *dd_phys 
    10 dmu_buf_t *dd_dbuf 
    18 dsl_pool_t *dd_pool 
    20 txg_node_t dd_dirty_link {
        20 struct txg_node *[4] tn_next 
        40 uint8_t [4] tn_member 
    }
    48 dsl_dir_t *dd_parent 
    50 kmutex_t dd_lock {
        50 void *[1] _opaque 
    }
    58 list_t dd_prop_cbs {
        58 size_t list_size 
        60 size_t list_offset 
        68 struct list_node list_head {
            68 struct list_node *list_next 
            70 struct list_node *list_prev 
        }
    }
    78 timestruc_t dd_snap_cmtime {
        78 time_t tv_sec 
        80 long tv_nsec 
    }
    88 uint64_t dd_origin_txg 
    90 uint64_t [4] dd_tempreserved 
    b0 int64_t [4] dd_space_towrite 
    d0 uint64_t dd_used_bytes 
    d8 uint64_t dd_compressed_bytes 
    e0 uint64_t dd_uncompressed_bytes 
    e8 uint64_t [5] dd_used_breakdown 
    110 char [256] dd_myname 
}

# echo "::enum arc_flags_t" | mdb -k
VALUE       NAME
1           ARC_FLAG_METADATA
2           ARC_FLAG_FREQUENTLY_USED
4           ARC_FLAG_HASHED
8           ARC_FLAG_PREFETCHED
16          ARC_FLAG_GHOST
32          ARC_FLAG_BP_CKSUM_INUSE
64          ARC_FLAG_DONT_L2CACHE
128         ARC_FLAG_L2BUF
256         ARC_FLAG_HOLE
512         ARC_FLAG_READ_IN_PROGRESS
1024        ARC_FLAG_WRITE_RACE

# echo "::enum arc_options_t" | mdb -k
VALUE       NAME
1           ARC_OPT_METADATA
2           ARC_OPT_NOL2CACHE
4           ARC_OPT_REPORTCACHED

# echo "::enum zio_flag" | mdb -k
VALUE       NAME
1           ZIO_FLAG_DONT_AGGREGATE
2           ZIO_FLAG_IO_REPAIR
4           ZIO_FLAG_SELF_HEAL
8           ZIO_FLAG_RESILVER
16          ZIO_FLAG_SCRUB
32          ZIO_FLAG_SCAN_THREAD
64          ZIO_FLAG_CANFAIL
128         ZIO_FLAG_SPECULATIVE
256         ZIO_FLAG_CONFIG_WRITER
512         ZIO_FLAG_DONT_RETRY
1024        ZIO_FLAG_DONT_CACHE
2048        ZIO_FLAG_NODATA
4096        ZIO_FLAG_INDUCE_DAMAGE
8192        ZIO_FLAG_IO_RETRY
16384       ZIO_FLAG_PROBE
32768       ZIO_FLAG_TRYHARD
65536       ZIO_FLAG_OPTIONAL
131072      ZIO_FLAG_DONT_QUEUE
262144      ZIO_FLAG_DONT_PROPAGATE
524288      ZIO_FLAG_IO_BYPASS
1048576     ZIO_FLAG_IO_REWRITE
2097152     ZIO_FLAG_RAW
4194304     ZIO_FLAG_GANG_CHILD
8388608     ZIO_FLAG_DDT_CHILD
16777216    ZIO_FLAG_GODFATHER
33554432    ZIO_FLAG_MVECTOR
67108864    ZIO_FLAG_JOINED

# echo "::enum arc_state_enum_t" | mdb -k
VALUE       NAME
0           ARC_STATE_MRU
1           ARC_STATE_MFU
2           ARC_STATE_MRU_GHOST
3           ARC_STATE_MFU_GHOST

# echo "::enum dmu_object_type_t" | mdb -k
VALUE       NAME
0           DMU_OT_NONE
1           DMU_OT_OBJECT_DIRECTORY
2           DMU_OT_OBJECT_ARRAY
3           DMU_OT_PACKED_NVLIST
4           DMU_OT_PACKED_NVLIST_SIZE
5           DMU_OT_BPOBJ
6           DMU_OT_BPOBJ_HDR
7           DMU_OT_SPACE_MAP_HEADER
8           DMU_OT_SPACE_MAP
9           DMU_OT_INTENT_LOG
10          DMU_OT_DNODE
11          DMU_OT_OBJSET
12          DMU_OT_DSL_DIR
13          DMU_OT_DSL_DIR_CHILD_MAP
14          DMU_OT_DSL_DS_SNAP_MAP
15          DMU_OT_DSL_PROPS
16          DMU_OT_DSL_DATASET
17          DMU_OT_ZNODE
18          DMU_OT_OLDACL
19          DMU_OT_PLAIN_FILE_CONTENTS
20          DMU_OT_DIRECTORY_CONTENTS
21          DMU_OT_MASTER_NODE
22          DMU_OT_UNLINKED_SET
23          DMU_OT_ZVOL
24          DMU_OT_ZVOL_PROP
25          DMU_OT_PLAIN_OTHER
26          DMU_OT_UINT64_OTHER
27          DMU_OT_ZAP_OTHER
28          DMU_OT_ERROR_LOG
29          DMU_OT_SPA_HISTORY
30          DMU_OT_SPA_HISTORY_OFFSETS
31          DMU_OT_POOL_PROPS
32          DMU_OT_DSL_PERMS
33          DMU_OT_ACL
34          DMU_OT_SYSACL
35          DMU_OT_FUID
36          DMU_OT_FUID_SIZE
37          DMU_OT_NEXT_CLONES
38          DMU_OT_SCAN_QUEUE
39          DMU_OT_USERGROUP_USED
40          DMU_OT_USERGROUP_QUOTA
41          DMU_OT_USERREFS
42          DMU_OT_DDT_ZAP
43          DMU_OT_DDT_STATS
44          DMU_OT_SA
45          DMU_OT_SA_MASTER_NODE
46          DMU_OT_SA_ATTR_REGISTRATION
47          DMU_OT_SA_ATTR_LAYOUTS
48          DMU_OT_SCAN_XLATE
49          DMU_OT_DEDUP
50          DMU_OT_DEADLIST
51          DMU_OT_DEADLIST_HDR
52          DMU_OT_DSL_CLONES
53          DMU_OT_BPOBJ_SUBOBJ
54          DMU_OT_DSL_KEYCHAIN
55          DMU_OT_NUMTYPES

$ nl -b a -n rz usr/src/uts/common/sys/fs/zfs.h | sed -n '/zfs_cache_type/,/}/p'
000297	typedef enum zfs_cache_type {
000298		ZFS_CACHE_NONE = 0,
000299		ZFS_CACHE_METADATA = 1,
000300		ZFS_CACHE_ALL = 2
000301	} zfs_cache_type_t;
*/

fbt:zfs:dbuf_evict:entry, fbt:zfs:dbuf_evict_user:entry, fbt:zfs:dbuf_is_metadata:entry
	args[0]: dmu_buf_impl_t *

fbt:zfs:dbuf_rele:entry, fbt:zfs:dbuf_rele_and_unlock:entry
	args[0]: dmu_buf_impl_t *
	args[1]: void *

fbt:zfs:dbuf_read:entry
	args[0]: dmu_buf_impl_t *
	args[1]: zio_t *
	args[2]: uint32_t

fbt:zfs:arc_destroy_ref:entry, fbt:zfs:arc_evict_ref:entry, fbt:zfs:arc_remove_ref:entry, fbt:zfs:arc_reactivate_ref:entry
	args[0]: arc_ref_t *

fbt:zfs:arc_inactivate_ref:entry
	args[0]: arc_ref_t *
	args[1]: arc_evict_func_t *
	args[2]: void *

fbt:zfs:arc_data_free:entry, fbt:zfs:arc_add_to_evictables:entry
	args[0]: arc_buf_t *

fbt:zfs:arc_kill_buf:entry
	args[0]: arc_buf_t *
	args[1]: boolean_t

fbt:zfs:arc_hold:entry, fbt:zfs:arc_rele:entry
	args[0]: arc_buf_t *
	args[1]: void *

'

/bin/false <<EOF
EOF
