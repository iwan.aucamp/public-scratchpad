#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int function_aaa()
{
	time_t rv = time( NULL );
	rv = rv - 100;
	return rv;
}

int function_aab()
{
	int rv=100;
	rv = rv - 10;
	return rv;
}

int function_aac( int ia, int ib, int ic )
{
	int rv= ia + ib + ic;
	rv = rv - 10;
	return rv;
}

int function_aad( int i_a, int i_b, int i_c, int i_d, int i_e, int i_f, int i_g, int i_h, int i_i )
{
	int rv = i_a + i_b + i_c + i_d + i_e + i_f + i_g + i_h + i_i;
	rv = rv - 10;
	return rv;
}

int function_aae( int* ia )
{
	int rv = *ia;
	rv = rv - 10;
	return rv;
}

int main()
{
	int discard = -1;
	discard = function_aaa();
	printf( "discard = %d\n", discard );
	discard = function_aab();
	printf( "discard = %d\n", discard );
	discard = function_aac( 1, 100, 1000 );
	printf( "discard = %d\n", discard );
	discard = function_aad( 2, 4, 6, 8, 10, 12, 14, 16, 18 );
	printf( "discard = %d\n", discard );
	int aaev = 123;
	discard = function_aae( &aaev );
	printf( "discard = %d\n", discard );
	return EXIT_SUCCESS;
}
