#!/bin/bash

script_basename=$(basename "${0}")
script_dirname=$(dirname "${0}")

expand_boolean()
{
	local boolean_name=${1}
	local boolean_value
	eval "boolean_value=\${${boolean_name}}"
	if "${boolean_value}"
	then
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

expand_test()
{
	local boolean_name=${1}
	shift 1
	if "${@}"
	then
		eval "${boolean_name}=true"
		eval "${boolean_name}_true=1"
		eval "${boolean_name}_false=''"
	else
		eval "${boolean_name}=false"
		eval "${boolean_name}_false=1"
		eval "${boolean_name}_true=''"
	fi
}

do_if_true()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		"${@}"
		return "${?}"
	else
		return 1
	fi
}

do_if_false()
{
	local value="${1}"
	shift 1
	if "${value}"
	then
		return 1
	else
		"${@}"
		return "${?}"
	fi
}

verbosity_to_flags()
{
	local flag="${1}"
	local remainder="${verbosity:--1}"
	while [ "${remainder}" -gt "-1" ]
	do
		remainder=$(( ${remainder} - 1 ))
		echo -n " ${flag}"
	done
	echo
}

verbose_echo()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
	echo "${@}" 1>&2
	return 0
}

verbose_dump_vars()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_vars_msg()
{
	local level="${1}"
	local msg="${2}"
	shift 2
	test "${verbosity:--1}" -ge "${level}" || return 0
	for variable in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ ${msg} " 1>&2
		declare -p "${variable}" 1>&2
	done
	return 0
}

verbose_dump_functions()
{
	local level="${1}"
	shift 1
	test "${verbosity:--1}" -ge "${level}" || return 0
	for function in "${@}"
	do
		echo -n "$(date +%Y-%m-%dT%H:%M:%S) ${0}:$$ " 1>&2
		declare -f "${function}" 1>&2
	done
	return 0
}

error_echo()
{
	echo "${@}" 1>&2
}

arcsnoop_main_usage()
{
	stty tabs
	test "${#}" -ge 1 && { error_echo "ERROR: ${@}"; }
	local align="\r\t\t\t"
	error_echo -e "${script_basename} [options] [interval] [count]"
	error_echo -e "options:"
	error_echo -e " -P predicate${align}predicate to use when tracing"
	error_echo -e " -v${align}increase verbosity"
	error_echo -e " -d${align}pretend mode"
	error_echo -e " -u depth${align}user stack depth"
	error_echo -e " -k depth${align}kernel stack depth"
	error_echo -e " -f (all|data|meta)${align}data filter"
	error_echo -e " -l (all|hit|miss)${align}location filter"
	error_echo -e " -t${align}trace"
	error_echo -e " -s${align}speculative tracing"
	error_echo -e " -S${align}speculative tracing + debug"
	error_echo -e " -w seconds${align}trace duration"
	error_echo -e " -E${align}trace evicts"
	error_echo -e " -R${align}trace reads"
	error_echo -e " -I${align}enable probesets without predicates"
	error_echo -e " -i${align}enable probesets with predicates"
	return 0;
}

expand_probesets()
{
	local probesets=( "${@}" )
	local probes=""
	for probeset_name in "${probesets[@]}"
	do
		local probeset=""
		eval 'probeset="${probeset_'${probeset_name}'}"'
		verbose_dump_vars 1 probeset
		test -n "${probeset}" || { error_echo "Invalid probeset name ${probeset_name}"; return 1;}
		probes="${probes:+${probes}$',\n'}${probeset}"
	done
	echo "${probes}"
	return 0
}

arcsnoop_main()
{
	local argv=( "${@}" )
	local verbosity=-1
	local verbose=false
	local pretend=false

	verbose_echo 1 "${FUNCNAME}: @=${@}"
	local OPTARG OPTERR option OPTIND
	while getopts "hvdP:u:k:tf:l:sSc:w:o:Di:I:" option "${@}"
	do
		case "${option}" in
			h)
				"${FUNCNAME}_usage"
				return 0;
				;;
			v)
				verbosity=$(( ${verbosity} + 1 ))
				;;
			d)
				pretend=true;
				;;
			*)
				"${FUNCNAME}_usage" "Invalid options [ OPTARG=${OPTARG}, OPTERR=${OPTERR}, option=${option} ]"
				exit 1
				;;
		esac
	done
	test "${verbosity}" -ge 0 && { verbose=true; }
	verbose_dump_vars 1 script_dirname script_basename argv OPTERR option OPTIND
	verbose_dump_vars 1 verbosity pretend
	shift $(( ${OPTIND} - 1 ))

	local pretend_true pretend_false
	expand_boolean pretend



	program='
'

	echo "${program}" | gawk '{ printf( "%04d:\t\t%s\n", NR, $0 ) }' 1>&2
	echo "${program}" | /usr/sbin/dtrace -64 -q -s /dev/stdin ${output_file_set_true:+-o ${output_file}}
}

arcsnoop_main "${@}"

exit 0;

__comment='
'

/bin/false <<EOF
EOF
