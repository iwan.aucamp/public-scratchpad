#!/usr/bin/env node
// vim: set sts=4 ts=4 sw=4 filetype=javascript noexpandtab:

Object.defineProperty(global, '__stack', {
get: function() {
		var orig = Error.prepareStackTrace;
		Error.prepareStackTrace = function(_, stack) {
			return stack;
		};
		var err = new Error;
		Error.captureStackTrace(err, arguments.callee);
		var stack = err.stack;
		Error.prepareStackTrace = orig;
		return stack;
	}
});

Object.defineProperty(global, '__line', {
get: function() {
		return __stack[1].getLineNumber();
	}
});

Object.defineProperty(global, '__function', {
get: function() {
		return __stack[1].getFunctionName();
	}
});

//var joint = require( "jointjs" );
var joint_path = require.resolve( "jointjs" );
var jsdom = require( "jsdom" );
var multiline = require( "multiline" );
//var jquery = require( "jquery" );
var jquery_path = require.resolve( "jquery" );
const document_string = multiline(function(){/*
<!doctype html>
	<head>
		<style>
			#paper {
				display: inline-block;
				border: 1px solid gray;
			}
		</style>
	</head>
	<body>
		<div id="paper"></div>
	</body>
</html>
*/});

//var document = jsdom.jsdom( document_string );
//var window = document.defaultView;
//var $ = jquery( window );

console.log( 'jquery_path = ' + jquery_path );
console.log( 'joint_path = ' + joint_path );

jsdom.env( document_string,
	[
		joint_path,
		jquery_path
	],
	function (err, window) {
try {
//console.log( "window.document.body.innerHTML = " + window.document.body.innerHTML );
var $ = window.$;
console.log( '$("#paper") = ' + $("#paper") );

console.log( "WINDOW:ENTRY" );

console.log( __line );

var graph = new joint.dia.Graph();

console.log( __line );

var paper = new joint.dia.Paper({
	el: $("#paper"),
	width: 800,
	height: 600,
	gridSize: 1,
	model: graph
});

console.log( __line );

var uml = joint.shapes.uml;

console.log( __line );

var states = {

	s0: new uml.StartState({
		position: { x:20 , y: 20 },
		size: { width: 30, height: 30 },
		attrs: {
			'circle': {
				fill: '#4b4a67',
				stroke: 'none'
			}
		}
	}),

	s1: new uml.State({
		position: { x:100 , y: 100 },
		size: { width: 200, height: 100 },
		name: "state 1",
		events: ["entry / init()","exit / destroy()"],
		attrs: {
			'.uml-state-body': {
				fill: 'rgba(48, 208, 198, 0.1)',
				stroke: 'rgba(48, 208, 198, 0.5)',
				'stroke-width': 1.5
			},
			'.uml-state-separator': {
				stroke: 'rgba(48, 208, 198, 0.4)'
			}
		}
	}),

	s2: new uml.State({
		position: { x:400 , y: 200 },
		size: { width: 300, height: 300 },
		name: "state 2",
		events: ["entry / create()","exit / kill()","A / foo()","B / bar()"],
		attrs: {
			'.uml-state-body': {
				fill: 'rgba(48, 208, 198, 0.1)',
				stroke: 'rgba(48, 208, 198, 0.5)',
				'stroke-width': 1.5
			},
			'.uml-state-separator': {
				stroke: 'rgba(48, 208, 198, 0.4)'
			}
		}
	}),

	s3: new uml.State({
		position: { x:130 , y: 400 },
		size: { width: 160, height: 60 },
		name: "state 3",
		events: ["entry / create()","exit / kill()"],
		attrs: {
			'.uml-state-body': {
				fill: 'rgba(48, 208, 198, 0.1)',
				stroke: 'rgba(48, 208, 198, 0.5)',
				'stroke-width': 1.5
			},
			'.uml-state-separator': {
				stroke: 'rgba(48, 208, 198, 0.4)'
			}
		}
	}),

	s4: new uml.State({
		position: { x:530 , y: 400 },
		size: { width: 160, height: 50 },
		name: "sub state 4",
		events: ["entry / create()"],
		attrs: {
			'.uml-state-body': {
				fill: 'rgba(48, 208, 198, 0.1)',
				stroke: 'rgba(48, 208, 198, 0.5)',
				'stroke-width': 1.5
			},
			'.uml-state-separator': {
				stroke: 'rgba(48, 208, 198, 0.4)'
			}
		}
	}),

	se: new uml.EndState({
		position: { x:750 , y: 550 },
		size: { width: 30, height: 30 },
		attrs: {
			'.outer': {
				stroke: "#4b4a67",
				'stroke-width': 2
			},
			'.inner': {
				fill: '#4b4a67'
			}
		}
	})

};

console.log( __line );

graph.addCells(states);

states.s2.embed(states.s4);

var linkAttrs = {
	'fill': 'none',
	'stroke-linejoin': 'round',
	'stroke-width': '2',
	'stroke': '#4b4a67'
};

var transitons = [
	new uml.Transition({
		source: { id: states.s0.id },
		target: { id: states.s1.id },
		attrs: {'.connection': linkAttrs }
	}),
	new uml.Transition({
		source: { id: states.s1.id },
		target: { id: states.s2.id },
		attrs: {'.connection': linkAttrs }
	}),
	new uml.Transition({
		source: { id: states.s1.id },
		target: { id: states.s3.id },
		attrs: {'.connection': linkAttrs }
	}),
	new uml.Transition({
		source: { id: states.s3.id },
		target: { id: states.s4.id },
		attrs: {'.connection': linkAttrs }
	}),
	new uml.Transition({
		source: { id: states.s2.id },
		target: { id: states.se.id },
		attrs: {'.connection': linkAttrs }
	})
];

console.log( __line );

graph.addCells(transitons);

console.log( graph.toSVG() );

console.log( "WINDOW:END" );
}
catch( exception )
{
	console.log( "caught exception:" + exception );
	console.log( exception.stack );
}

});
