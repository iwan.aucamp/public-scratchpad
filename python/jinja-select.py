#!/usr/bin/env python2

import json
import jinja2

def main():
	data_json = """
	{
		"results": [
			{
				"changed": false,
				"item": {
					"chmod": "u=rw,go=r",
					"name": "etc/hosts"
				}
			},
			{
				"changed": false,
				"item": {
					"chmod": "u=rw,go=r",
					"name": "etc/yum.repos.d/"
				}
			},
			{
				"changed": false,
				"item": {
					"chmod": "u=rw,go=r",
					"name": "etc/ssh/ssh_known_hosts"
				}
			},
			{
				"changed": false,
				"item": {
					"chmod": "u=rw,go=r",
					"name": "etc/profile.d/"
				}
			},
			{
				"changed": true,
				"item": {
					"chmod": "u=rw,go=r",
					"name": "etc/ntp.conf"
				}
			}
		]
	}"""

	data = json.loads( data_json )
	#template = jinja2.Template( "RESULT:\n{{ sync_global_files.results | selectattr( 'item.name', 'etc/ntp.conf' ) }}\n:RESULT\n" )
	print( "sync_global_files = {}\n".format( json.dumps( data_json, indent=2 ) ) )
	template_string = "{{ ( sync_global_files.results | selectattr( 'item.name', 'equalto', 'etc/ntp.confx' ) | list )[0].changed }}"
	template_string = "{% set ntpd_conf_result = sync_global_files.results | selectattr( 'item.name', 'equalto', 'etc/ntp.conf' ) | list %}{{ ntpd_conf_result[0].changed if ntpd_conf_result else false }}"
	print( "template_string = {}\n".format( template_string ) )
	template = jinja2.Template( template_string )
	print( "template_output = {}\n".format( template.render( sync_global_files = data ) ) )

if __name__ == "__main__":
	main()

