#!/usr/bin/env bash

last_signal=""
running=true

signal_handler()
{
	local signal="${1}"
	echo "${FUNCNAME[0]}: signal=${signal}"
	last_signal="${signal}"
	case "${signal}" in
		TERM|INT)
			running=false
			echo "${FUNCNAME[0]}: stopping"
			;;
		CONT)
			echo "${FUNCNAME[0]}: forwarding ..."
			trap "-" CONT
			trap "signal_handler TSTP" TSTP
			kill -${signal} "${$}"
			;;
		TSTP)
			echo "${FUNCNAME[0]}: forwarding ..."
			trap "-" TSTP
			trap "signal_handler CONT" CONT
			kill -${signal} "${$}"
			;;
		*)
			echo "${FUNCNAME[0]}: ignoring ..."
			;;
	esac
}

for trap_signal in TSTP STOP CONT TERM INT EXIT
do
	trap "signal_handler ${trap_signal}" ${trap_signal}
done

echo "this.pid = ${$}"

sleep 20 &
pid="${!}"
while kill -0 "${pid}" >/dev/null
do
	! "${running}" && {
		echo "stoppping ..."
		break
	}
	echo "waiting for ${pid}"
	wait "${pid}"
	rc="${?}"
	echo "wait returned ${?}"
done
