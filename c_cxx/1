#include <string>
#include <iostream>
#include <memory>
#include <atomic>

#ifdef WITH_XERCES

#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#endif

template <typename Type>
using unique_ptr_d = std::unique_ptr< Type, std::function< void( Type* ) > >;

class function_boundry_helper
{
public:
	typedef std::function< void( std::ostream& )> boundry;
private:
	std::string m_name;
	static boundry blank_boundry;
	boundry m_return;
	static std::atomic_int m_indent;
public:
	function_boundry_helper( const std::string& name, boundry entry = blank_boundry, boundry return_ = blank_boundry )
		:
			m_name( name ),
			m_return( return_ )
	{
		for ( int i = 0; i < function_boundry_helper::m_indent; ++i ) std::cerr << "  ";
		function_boundry_helper::m_indent++;

		std::cerr << " -> " << this->m_name << ":entry";
		if ( entry ) entry( std::cerr );
		std::cerr << std::endl;
	}

	~function_boundry_helper()
	{
		function_boundry_helper::m_indent--;
		for ( int i = 0; i < function_boundry_helper::m_indent; ++i ) std::cerr << "  ";

		std::cerr << " <- " << this->m_name << ":return";
		if ( this->m_return ) this->m_return( std::cerr );
		std::cerr << std::endl;
	}
};

void tassert_fail( const char *expr, const char* file, int line, const char* function )
{
	throw std::runtime_error( std::string( file ) + ":" + std::to_string( line ) + ":" + function + ": assertion failed " + expr );
}

#define tassert( expr ) \
	( (expr) ? (void)0 : tassert_fail( #expr, __FILE__, __LINE__, __PRETTY_FUNCTION__ );

function_boundry_helper::boundry function_boundry_helper::blank_boundry = function_boundry_helper::boundry();
std::atomic_int function_boundry_helper::m_indent( 0 );
typedef function_boundry_helper fbh;

class some_object
{
	std::string m_value;
	void deleter( const void* ) {}
public:
	some_object( const std::string& value = "undef" )
		:
			m_value( value )
	{
		fbh __fbh( __PRETTY_FUNCTION__ );
	}
	~some_object()
	{
		fbh __fbh( __PRETTY_FUNCTION__ );
	}
public:
};

class some_factory
{
public:
	some_object* create( const std::string& value = "undef" ) const
	{
		fbh __fbh( __PRETTY_FUNCTION__ );
		return new some_object( value );
	}
	void destroy( some_object* object ) const
	{
		fbh __fbh( __PRETTY_FUNCTION__ );
		delete object;
	}
};

some_object* create_function( const std::string& value = "undef" )
{
	fbh __fbh( __PRETTY_FUNCTION__ );
	return new some_object( value );
}

void destroy_function( some_object* object )
{
	fbh __fbh( __PRETTY_FUNCTION__ );
	delete object;
}

#ifdef WITH_XERCES

template < typename ValueType, const std::function< void( ValueType* ) >& release >
class use_once
{
	std::unique_ptr< ValueType, std::function< void( ValueType* ) > > m_value;
public:
	use_once( ValueType* value )
		:
			m_value( value, release )
	{
		fbh __fbh( __PRETTY_FUNCTION__, [&]( std::ostream& o ) { o << ": this->m_value" << this->m_value.get() << std::endl; } );
	}
	const ValueType* u() const
	{
		fbh __fbh( __PRETTY_FUNCTION__, [&]( std::ostream& o ) { o << ": this->m_value" << this->m_value.get() << std::endl; } );
		return this->m_value.get();
	}
	~use_once()
	{
		fbh __fbh( __PRETTY_FUNCTION__, [&]( std::ostream& o ) { o << ": this->m_value" << this->m_value.get() << std::endl; } );
	}
};

std::function< void( XMLCh* ) > xsuor = []( XMLCh* p ){ xercesc_3_1::XMLString::release( &p ); };
typedef use_once< XMLCh, xsuor > xsuo;

std::function< void( char* ) > psuor = []( char* p ){ xercesc_3_1::XMLString::release( &p ); };
typedef use_once< char, psuor > psuo;

#define PTX( ps ) ( xsuo( xercesc_3_1::XMLString::transcode( ps ) ).u() )
#define XTP( xs ) ( psuo( xercesc_3_1::XMLString::transcode( xs ) ).u() )

template < typename ObjectType >
class call_release_on
{
public:
	void operator()( ObjectType* object_pointer )
	{
		object_pointer->release();
	}
};

template < typename ObjectType >
using unique_ptr_r = std::unique_ptr< ObjectType, call_release_on >;

#endif

int main()
{
	try
	{

	fbh __fbh( __PRETTY_FUNCTION__ );

	std::cout << std::endl << "Create directly using new ... will get destoryed with delete:" << std::endl << std::endl;

	{
		std::unique_ptr< some_object > xa( new some_object );
	}

	std::cout << std::endl << "Create and destroy via factory class:" << std::endl << std::endl;

	{
		some_factory factory;
		std::unique_ptr< some_object, std::function< void( some_object* ) > > xb(
			factory.create( "xb" ), std::bind( &some_factory::destroy, &factory, std::placeholders::_1 ) );
	}

	std::cout << std::endl << "Create and destroy via factory (create/destroy) functions:" << std::endl << std::endl;

	{
		std::unique_ptr< some_object, std::function< void( some_object* ) > > xc(
			create_function( "xc" ), std::bind( &destroy_function, std::placeholders::_1 ) );
	}

	std::cout << std::endl << "Deleting void* will not call destructor (also is undefined behaviour):" << std::endl << std::endl;

	{
		void* xd = new some_object;
		delete xd;
	}

#ifdef WITH_XERCES
	using namespace xercesc_3_1;
	std::cout << std::endl << "With xerces" << std::endl << std::endl;

	XMLPlatformUtils::Initialize();

	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation( PTX( "Core" ) );

	unique_ptr_d< DOMDocument > doc( impl->createDocument( 0, PTX( "test" ), 0 ) );
	doc->appendChild( doc->createElement( PTX( "some" ) ) );
	doc->appendChild( doc->createElement( PTX( "stuff" ) ) );

#endif

	std::cout << std::endl;

	}
	catch( const std::exception& e )
	{
		std::cerr << "caught:" << e.what() << std::endl;
		throw;
	}
#ifdef WITH_XERCES
	catch( const xercesc_3_1::DOMException& e )
	{
		std::cerr << "caught:" << XTP( e.getMessage() ) << " : " << e.code << std::endl;
		throw;
	}
#endif

	return EXIT_SUCCESS;
}

