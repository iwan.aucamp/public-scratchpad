#include <iostream>
#include <dlfcn.h>
#include <stdexcept>

int main( int argc, char* argv[] )
{
	try
	{
		std::cerr << __PRETTY_FUNCTION__ << ":entry" << std::endl;
		for( int i = 0; i < argc; ++i )
		{
			std::cerr << __PRETTY_FUNCTION__ << ":argv[ " << i << " ] = " << argv[i] << std::endl;
		}
		if ( argc < 2 ) throw std::runtime_error( "Need at least 1 argument" );

		dlerror();
		void* dl_handle = dlopen( argv[1], RTLD_LAZY );
		if ( !dl_handle )
		{
			throw std::runtime_error( std::string() + "Could not dlopen() '" + argv[1] + "' : " + dlerror() );
		}
		dlerror();
		int ( *dl_main )( int argc, char* argv[] ) = ( int (*)(int, char**) ) dlsym( dl_handle, "main" );
		if ( !dl_main )
		{
			throw std::runtime_error( std::string() + "Could not dlsym() '" + "main" + "' : " + dlerror() );
		}
		return dl_main( argc - 1, &argv[1] );
	}
	catch( const std::exception& e )
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}
}
