#include <csignal>
#include <iostream>
#include <string>
#include <atomic>
#include <chrono>
#include <thread>

/*
#ifdef WITH_INTERP
const char interp_section[] __attribute__((section(".interp"))) = "/lib64/ld-linux-x86-64.so.2";
#endif
*/

namespace {
volatile std::atomic_bool g_stopped;
}

extern "C" {
int main( int argc, char* argv[] );
}

void signal_handler_stop( int signal )
{
	std::cerr << __PRETTY_FUNCTION__ << ": signal = " << signal << std::endl;
	g_stopped = true;
}


class my_class
{
public:
	my_class( const std::string& value )
		:
			m_value( value )
	{
		std::cerr << __PRETTY_FUNCTION__ << ": this = " << this << std::endl;
	}
	~my_class()
	{
		std::cerr << __PRETTY_FUNCTION__ << ": this = " << this << std::endl;
	}
private:
	std::string m_value;
};

int main( int argc, char* argv[] )
{
	(void)argc;
	(void)argv;
	g_stopped = false;
	std::signal( SIGINT, signal_handler_stop );
	std::signal( SIGTERM, signal_handler_stop );
	unsigned int i = 0;
	while ( !g_stopped )
	{
		my_class* discard = new my_class( "xyz" );
		std::cerr << "iteration " << i++ << " : discard = " << discard << std::endl;
		std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
	}
	std::cerr << "done ..." << std::endl;
	return EXIT_SUCCESS;
}

/*
#ifdef WITH_MAIN
int main( int argc, char* argv[] )
{
	return xmain( argc, argv );
}
#endif
*/
